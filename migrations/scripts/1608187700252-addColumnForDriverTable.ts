import {MigrationInterface, QueryRunner} from "typeorm";

export class addColumnForDriverTable1608187700252 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE drivers ADD COLUMN workingStatus varchar(255) DEFAULT NULL
        `)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
