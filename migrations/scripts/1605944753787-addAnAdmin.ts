import { getConnection, MigrationInterface, QueryRunner } from "typeorm";
import { Users } from '../../src/domains/users/models/users.schema'
import { UserRoles } from '../../src/domains/user_roles/models/user_roles.schema'
import { ConfigsService } from '../../src/configs/configs.service'
import * as bcrypt from 'bcrypt'

export class addAnAdmin1605944753787 implements MigrationInterface {

    public async up(): Promise<void> {
        const configsService = new ConfigsService()

        const connection = await getConnection()
        const userRepo = connection.getRepository<Users>('Users')
        const userRoleRepo = connection.getRepository<UserRoles>('UserRoles')
        const hashPass = await bcrypt.hash('Hieu27771999', configsService.bcryptSalt)

        const admin = await userRepo.save({
            firstName: 'Tran',
            lastName: 'Hieu',
            phoneNumber: '+843451995113',
            status: 'ACTIVE',
            password: hashPass,
        })

        await userRoleRepo.save({
            userID: admin.userID,
            roleName: 'ADMIN',
            status: 'ACTIVE'
        })

        await connection.close
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
