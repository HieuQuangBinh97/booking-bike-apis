export enum scopes  {
  USERS_CREATE = 'users:create',
  USERS_READ = 'users:read',
  USERS_UPDATE = 'users:update',
  USERS_DELETE = 'users:delete',

  DRIVER_CREATE = 'driver:create',
  DRIVER_READ = 'driver:read',
  DRIVER_UPDATE = 'driver:update',
  DRIVER_DELETE = 'driver:delete',
  DRIVER_CONFIRM = 'driver:confirm',

  VEHICLE_CREATE = 'vehicle:create',
  VEHICLE_READ = 'vehicle:read',
  VEHICLE_UPDATE = 'vehicle:update',
  VEHICLE_DELETE = 'vehicle:delete',

  VEHICLE_BOOKING_CREATE = 'vehicle_booking:create',
  VEHICLE_BOOKING_READ = 'vehicle_booking:read',
  VEHICLE_BOOKING_UPDATE = 'vehicle_booking:update',
  VEHICLE_BOOKING_DELETE = 'vehicle_booking:delete',
  VEHICLE_BOOKING_DRIVER_CONFIRM = 'vehicle_booking:driver:confirm',
  VEHICLE_BOOKING_DRIVER_UPDATE = 'vehicle_booking:driver:update',
  VEHICLE_BOOKING_ACCEPT = 'vehicle_booking:accept',

  FEEDBACK_CREATE = 'feedback:create',
  FEEDBACK_READ = 'feedback:read',
  FEEDBACK_UPDATE = 'feedback:update',
  FEEDBACK_DELETE = 'feedback:delete',

  NOTIFICATION_READ = 'notification:read',

  ADMIN_STATISTIC = 'admin:statistic'
}