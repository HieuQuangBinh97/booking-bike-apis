import { scopes } from "./scopes";

export const roles = {
  ADMIN: [
    scopes.ADMIN_STATISTIC
  ],
  BASIC: [
    scopes.DRIVER_CREATE,
    scopes.DRIVER_READ,
    scopes.DRIVER_UPDATE,

    scopes.USERS_READ,
    scopes.USERS_UPDATE,

    scopes.VEHICLE_BOOKING_CREATE,
    scopes.VEHICLE_BOOKING_UPDATE,
    scopes.VEHICLE_BOOKING_READ,

    scopes.FEEDBACK_READ,
    scopes.FEEDBACK_CREATE,
    scopes.FEEDBACK_UPDATE,
    scopes.FEEDBACK_DELETE,

    scopes.NOTIFICATION_READ
  ],
  MANAGEMENT_STAFF: [
    scopes.USERS_READ,
    scopes.USERS_UPDATE,

    scopes.DRIVER_CREATE,
    scopes.DRIVER_READ,
    scopes.DRIVER_UPDATE,
    scopes.DRIVER_DELETE,
    scopes.DRIVER_CONFIRM,

    scopes.VEHICLE_BOOKING_CREATE,
    scopes.VEHICLE_BOOKING_DELETE,
    scopes.VEHICLE_BOOKING_UPDATE,
    scopes.VEHICLE_BOOKING_READ,

    scopes.FEEDBACK_READ,
    scopes.FEEDBACK_UPDATE,
    scopes.FEEDBACK_DELETE,

    scopes.VEHICLE_CREATE,
    scopes.VEHICLE_READ,
    scopes.VEHICLE_UPDATE,
    scopes.VEHICLE_DELETE,
  ],
  DRIVER: [
    scopes.VEHICLE_BOOKING_ACCEPT,
    scopes.VEHICLE_BOOKING_DRIVER_CONFIRM,
    scopes.VEHICLE_BOOKING_DRIVER_UPDATE,
  ]
}