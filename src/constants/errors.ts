export const notFoundErrors = {
  UserNotFound: 'User not found'
}

export const badRequestErrors = {
}

export const forbiddenErrors = {
  ValidationError: 'No permisisons',
  Robot: 'Please verify by using recaptcha',
}
