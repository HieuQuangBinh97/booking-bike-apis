import * as dotenv from 'dotenv'
import * as Joi from 'joi'
import * as fs from 'fs'
import { extname } from 'path'

export interface EnvConfig {
  [key: string]: string
}

export class ConfigsService {
  private readonly envConfig: EnvConfig
  private readonly testingDbConfig: EnvConfig

  constructor() {
    let configFilePath = ''
    if (process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
      configFilePath = '.production.env'
    }

    if (!process.env.NODE_ENV) {
      configFilePath = '.local.env'
    }

    const config = dotenv.parse(fs.readFileSync(configFilePath))
    config.NODE_ENV = process.env.NODE_ENV || 'local'
    console.log('Node env', process.env.NODE_ENV)

    this.envConfig = this.validateInput(config)
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['production', 'local'])
        .default('local'),
      HOST: Joi.string().required(),
      PORT: Joi.number().required(),
      JWT_SECRET: Joi.string().required(),
      JWT_CONFIRM_SECRET: Joi.string().required(),
      BCRYPT_SALT: Joi.number().required(),
      REGEX: Joi.string().required(),
      TYPEORM_CONNECTION: Joi.string().required(),
      TYPEORM_HOST: Joi.string().required(),
      TYPEORM_USERNAME: Joi.string().required(),
      TYPEORM_PASSWORD: Joi.string().empty('').default(''),
      TYPEORM_DATABASE: Joi.string().required(),
      TYPEORM_PORT: Joi.number().required(),
      TYPEORM_SYNCHRONIZE: Joi.string().valid(['true', 'false']),
      TYPEORM_SCHEMA: Joi.string().valid(['true', 'false']),
      TYPEORM_ENTITIES: Joi.string().required(),
      TYPEORM_MIGRATIONS: Joi.string().required(),
      TYPEORM_MIGRATIONS_RUN: Joi.string().valid(['true', 'false']),
      TYPEORM_MIGRATIONS_DIR: Joi.string().required(),
      TYPEORM_DROP_SCHEMA: Joi.string().valid(['true', 'false']),
      TYPEORM_LOGGING: Joi.string().valid(['true', 'false']),
    })

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    )

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    const jwtConfirmSecret = String(validatedEnvConfig.JWT_CONFIRM_SECRET)
    const jwtSecret = String(validatedEnvConfig.JWT_SECRET)

    if (jwtConfirmSecret === jwtSecret) {
      throw new Error('Jwt secret key and jwt confirm secret key must be different')
    }

    return validatedEnvConfig
  }

  private validateDbInput(envConfig: EnvConfig): EnvConfig {
    const dbSchema: Joi.ObjectSchema = Joi.object({
      TYPEORM_CONNECTION: Joi.string().required(),
      TYPEORM_HOST: Joi.string().required(),
      TYPEORM_USERNAME: Joi.string().required(),
      // TYPEORM_PASSWORD: Joi.string(),
      TYPEORM_DATABASE: Joi.string().required(),
      TYPEORM_PORT: Joi.number().required(),
      TYPEORM_SYNCHRONIZE: Joi.string().valid(['true', 'false']),
      TYPEORM_SCHEMA: Joi.string().valid(['true', 'false']),
      TYPEORM_ENTITIES: Joi.string().required(),
      TYPEORM_MIGRATIONS: Joi.string().required(),
      TYPEORM_MIGRATIONS_RUN: Joi.string().valid(['true', 'false']),
      TYPEORM_MIGRATIONS_DIR: Joi.string().required(),
      TYPEORM_DROP_SCHEMA: Joi.string().valid(['true', 'false']),
      TYPEORM_LOGGING: Joi.string().valid(['true', 'false']),
    })

    const { error, value: validatedDbConfig } = Joi.validate(
      envConfig,
      dbSchema,
    )

    if (error) {
      throw new Error(`Config validation error: ${error.message}`)
    }

    return validatedDbConfig
  }

  get nodeEnv(): string {
    return String(this.envConfig.NODE_ENV)
  }

  get host(): string {
    return String(this.envConfig.HOST)
  }

  get port(): string {
    return String(this.envConfig.PORT)
  }

  get jwtSecret(): string {
    return String(this.envConfig.JWT_SECRET)
  }

  get jwtConfirmSecret(): string {
    return String(this.envConfig.JWT_CONFIRM_SECRET)
  }

  get bcryptSalt(): number {
    return Number(this.envConfig.BCRYPT_SALT)
  }

  get getRegex(): string {
    return String(this.envConfig.REGEX)
  }

  get databaseConfig(): object {
    const dbConfig = {
      type: String(this.envConfig.TYPEORM_CONNECTION),
      host: String(this.envConfig.TYPEORM_HOST),
      port: Number(this.envConfig.TYPEORM_PORT),
      username: String(this.envConfig.TYPEORM_USERNAME),
      password: String(this.envConfig.TYPEORM_PASSWORD) ? String(this.envConfig.TYPEORM_PASSWORD) : '',
      database: String(this.envConfig.TYPEORM_DATABASE),
      entities: [String(this.envConfig.TYPEORM_ENTITIES)],
      autoSchemaSync: String(this.envConfig.TYPEORM_SCHEMA) === 'true',
      synchronize: String(this.envConfig.TYPEORM_SYNCHRONIZE) === 'true',
      migrations: [String(this.envConfig.TYPEORM_MIGRATIONS)],
      migrationsRun: String(this.envConfig.TYPEORM_MIGRATIONS_RUN) === 'true',
      dropSchema: String(this.envConfig.TYPEORM_DROP_SCHEMA) === 'true',
      logging: String(this.envConfig.TYPEORM_LOGGING) === 'true',
      cli: {
        migrationsDir: String(this.envConfig.TYPEORM_MIGRATIONS_DIR),
      },
    }

    return Object(dbConfig)
  }
}
