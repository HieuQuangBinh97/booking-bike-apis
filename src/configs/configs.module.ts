import { Global, Module } from '@nestjs/common'
import { ConfigsService } from './configs.service';

@Global()
@Module({
  exports: [
    ConfigsService,
  ],
  providers: [
    {
      provide: ConfigsService,
      useValue: new ConfigsService(),
    },
  ],
})

export class ConfigsModule { }
