import { In, MoreThan, LessThan } from 'typeorm'

interface UpdatingArray {
  neededUpdate: string[]
  currentArray: string[]
}



interface BuildNotExistArray {
  checkArray: string[]
  currentArray: string[]
}

// convert roleID from object to string when roleID was joined
export const buildAccess = (access) => {
  const { roleID } = access

  const shouldReturnNull = !roleID || (typeof roleID === 'object' && !roleID.roleID)

  if (shouldReturnNull) {
    return null
  }

  if (roleID.roleID) {
    access.roleID = roleID.roleID
  }

  return access
}

export const buildFindingQuery = ({ query }) => {
  const { sortBy, limit, cursor, sortDirection = 'ASC', ...findingQuery } = query
  const validDirection: number = sortDirection === 'ASC' ? 1 : -1
  const hasPage = !!limit
  const sortingCondition = { [sortBy]: validDirection }

  for (const key in findingQuery) {
    if (!Array.isArray(findingQuery[key])) {
      continue
    }

    findingQuery[key] = In(findingQuery[key])
  }

  const findAllQuery = { ...findingQuery }

  if (!limit) {
    return {
      findAllQuery,
      findingQuery,
      sortingCondition,
      hasPage,
    }
  }

  if (!cursor) {
    return {
      sortingCondition,
      findingQuery,
      findAllQuery,
      hasPage,
    }
  }

  findingQuery[sortBy] = validDirection === 1 ? MoreThan(cursor) : LessThan(cursor)

  return {
    sortingCondition,
    findingQuery,
    findAllQuery,
    hasPage,
  }

}

export const buildAddingInArray = ({ neededUpdate, currentArray }: UpdatingArray) => {
  if (!currentArray || !Array.isArray(currentArray) || !currentArray.length) {

    return neededUpdate
  }

  if (!neededUpdate || !Array.isArray(neededUpdate) || !neededUpdate.length) {

    return currentArray
  }

  const notExistingInCurrent = neededUpdate.filter(element => !currentArray.includes(element))

  const updatedArray = [...notExistingInCurrent, ...currentArray]

  return updatedArray
}

export const buildRemovingInArray = ({ neededUpdate, currentArray }: UpdatingArray) => {

  if (!neededUpdate || !Array.isArray(neededUpdate) || !neededUpdate.length) {

    return currentArray
  }

  const updatedArray = currentArray.filter(element => {
    if (!neededUpdate.includes(element)) {
      return true
    }

    return false

  })

  return updatedArray
}


export const buildNotExistArray = ({ checkArray, currentArray }: BuildNotExistArray) => {
  if (!currentArray || !currentArray.length) {

    return checkArray
  }

  if (!checkArray || !checkArray.length) {

    return currentArray
  }

  const notExistedArray = checkArray.filter(each => !currentArray.includes(each))

  return notExistedArray
}
