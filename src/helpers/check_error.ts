import { InternalServerErrorException, BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common'
import { notFoundErrors, badRequestErrors, forbiddenErrors } from '../constants/errors'
import { ConfigsService } from '../configs/configs.service'

interface Error {
  message?: string
  stack?: string
  code?: string | number,
  name?: string
}

const configService = new ConfigsService()
const checkControllerErrorsInLocal = (error: Error): void => {
  if (error.code === 'ER_DUP_ENTRY' && error.name === 'QueryFailedError') {
    console.log(`${error.message} - ${error.stack}`)
    throw new BadRequestException('Duplicated email')
  }

  if (error.code === 403) {
    console.log(`${error.message} - ${error.stack}`)
    throw new ForbiddenException(forbiddenErrors[error.name])
  }

  if (error.code === 404) {
    console.log(`${error.message} - ${error.stack}`)
    throw new NotFoundException(error)
  }

  if (error.code === 400) {
    console.log(`${error.message} - ${error.stack}`)
    throw new BadRequestException(error)
  }

  console.log(`${error.message} - ${error.stack}`)

  throw new InternalServerErrorException({
    error: `${error.message} - ${error.stack}`,
  })
}

const checkControllerErrorsInProd = (error: Error): void => {
  if (error.code === 'ER_DUP_ENTRY' && error.name === 'QueryFailedError') {
    throw new BadRequestException('Duplicated email')
  }

  if (error.name === 'ValidationError' && error.code === 403) {
    throw new ForbiddenException('No permisisons')
  }

  if (error.code === 404) {
    throw new NotFoundException(notFoundErrors[error.name])
  }

  if (error.code === 400) {
    throw new BadRequestException(badRequestErrors[error.name])
  }

  throw new InternalServerErrorException('Something wrong happens')
}

export const checkControllerErrors = configService.nodeEnv === 'local'
  ? checkControllerErrorsInLocal
  : checkControllerErrorsInProd
