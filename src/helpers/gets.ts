export const getNextCursor = ({ data, sortBy }) => {
  let nextCursor = 'END'

  if (data.length) {
    nextCursor = data[data.length - 1].createdAt
  }

  if (data.length && sortBy) {
    nextCursor = data[data.length - 1][sortBy]
  }

  return nextCursor
}