export interface Credentials {
  userID?: string
  roleNames?: string[]
  scopes?: string[]
  driverIDs?: string[]
  isAdmin?: boolean
  isPublic?: boolean
}

interface ValidateAccessToSingle<T> {
  data: T,
  credentials: Credentials,
  authKey?: string
}

interface ValidateAccessToList<T> {
  data: T[]
  credentials: Credentials
}

export const validateAccessToSingle = <T>({ data, credentials, authKey }: ValidateAccessToSingle<T>) => {
  let authData = data[authKey] ? data[authKey] : data
  let hasDriverPermission = false
  let isCustomer = false
  let isCreator = false
  let isTargetUser = false
  let hasPersonPermission = false 

  const { userID, driverIDs } = credentials
  if (!credentials) {
    throw {
      code: 403,
      name: 'ValidationError'
    }
  }

  if (credentials.isAdmin ) {
    return true
  }

  if (credentials.isPublic) {
    return true
  }

  if (authData && authData.driverID) {
    let driverID = authData.driverID.driverID ? authData.driverID.driverID : authData.driverID

    if (driverIDs.includes(driverID)) {
      hasDriverPermission = true
    }
  }

  if (hasDriverPermission) {
    return true
  }

  if (authData && authData.customerID) {
    let customerID = authData.customerID.userID ? authData.customerID.userID : authData.customerID
    
    if (userID === customerID) {
      isCustomer = true
    }
  }

  if (authData && authData.createdBy) {
    let createdBy = authData.createdBy.userID ? authData.createdBy.userID : authData.createdBy

    if (userID === createdBy) {
      isCreator = true
    }
  }


  if (authData && authData.userID) {
    let personID = authData.userID.userID ? authData.userID.userID : authData.userID

    if (userID === personID) {
      isTargetUser = true
    }
  }

  if (authData && authData.targetUserID) {
    let targetUserID = authData.targetUserID.userID ? authData.targetUserID.userID : authData.targetUserID

    if (userID === targetUserID) {
      isTargetUser = true
    }
  }


  if (isTargetUser || isCustomer || isCreator) {
    hasPersonPermission = true
  }

  if (hasPersonPermission || hasDriverPermission) {
    return true
  }

  throw {
    code: 403,
    name: 'ValidationError'
  }
}

export const validateAccessToList = <T>({ data, credentials }: ValidateAccessToList<T>): { validData: T[], lengthOfValidData: number} => {
  if (!data || !data.length) {
    return {
      validData: [],
      lengthOfValidData: 0
    }
  }

  if (!credentials) {
    throw {
      code: 403,
      name: 'ValidationError'
    }
  }

  if (credentials.isAdmin ) {
    return  {
      validData: data,
      lengthOfValidData: data.length
    }
  }

  if (credentials.isPublic) {
    return  {
      validData: data,
      lengthOfValidData: data.length
    }
  }

  const validData: T[] = []
  let lengthOfValidData = 0

  data.forEach((each: any ) => {
    let authData = each
    let hasDriverPermission = false
    let isCustomer = false
    let isCreator = false
    let isTargetUser = false
    let hasPersonPermission = false 

    const { userID, driverIDs } = credentials

    if (authData && authData.driverID) {
      let driverID = authData.driverID.driverID ? authData.driverID.driverID : authData.driverID

      if (driverIDs.includes(driverID)) {
        hasDriverPermission = true
      }
    }

    if (authData && authData.customerID) {
      let customerID = authData.customerID.userID ? authData.customerID.userID : authData.customerID
      
      if (userID === customerID) {
        isCustomer = true
      }
    }

    if (authData && authData.createdBy) {
      let createdBy = authData.createdBy.userID ? authData.createdBy.userID : authData.createdBy

      if (userID === createdBy) {
        isCreator = true
      }
    }


    if (authData && authData.userID) {
      let personID = authData.userID.userID ? authData.userID.userID : authData.userID

      if (userID === personID) {
        isTargetUser = true
      }
    }

    if (authData && authData.targetUserID) {
      let targetUserID = authData.targetUserID.userID ? authData.targetUserID.userID : authData.targetUserID

      if (userID === targetUserID) {
        isTargetUser = true
      }
    }


    if (isTargetUser || isCustomer || isCreator) {
      hasPersonPermission = true
    }

    if (hasPersonPermission || hasDriverPermission) {
      validData.push(each)

      lengthOfValidData ++
    }
  })

  return {
    validData,
    lengthOfValidData
  }
}