import { InjectRepository } from '@nestjs/typeorm'
import { validateAccessToList, validateAccessToSingle } from '../../shared/validate_access'
import { EntityRepository, Repository } from 'typeorm'
import { ChangePasswordService, CheckExistUserService, CreateOneService, DeleteOneService, FindManyUsers, FindManyUsersService, FindOneService, UpdateOneService } from './models/users.interface'
import { Users } from './models/users.schema'
import { buildFindingQuery } from '../../helpers/build'
import { getNextCursor } from '../../helpers/gets'

@EntityRepository(Users)
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private readonly usersRepository: Repository<Users>
  ) {}

  private readonly validateAccessToSingle = validateAccessToSingle
  private readonly validateAccessToList = validateAccessToList

  async createOne({ createOneData, credentials }: CreateOneService): Promise<Users> {
    try {
      this.validateAccessToSingle({
        data: createOneData,
        credentials,
      })

      const newUser = await this.usersRepository.save(createOneData)

      return newUser
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async checkExistUser({ query, credentials }: CheckExistUserService): Promise<boolean> {
    try {
      const users = await this.usersRepository.find({
        where: query,
      })

      if (!users || !users.length) {
        return false
      }

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne({ query, credentials, getPassword = false }: FindOneService): Promise<Users> {
    try {
      const user = await this.usersRepository.findOne({
        where: query
      })

      if (!user) {
        throw {
          code: 404,
          name: 'UserNotFound'
        }
      }

      this.validateAccessToSingle({
        data: user,
        credentials,
      })

      if (!getPassword) {
        delete user.password
      }
      
      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ query, updateData, credentials }: UpdateOneService): Promise<boolean> {
    try {
      const user = await this.usersRepository.findOne({
        where: query
      })     

      if (!user) {
        throw {
          code: 404,
          name: 'UserNotFound'
        }
      }

      this.validateAccessToSingle({
        data: user,
        credentials,
      })

      await this.usersRepository.save({
        ...user,
        ...updateData,
      })

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async changePassword({ query, password, credentials }: ChangePasswordService): Promise<boolean> {
    try {
      const user = await this.usersRepository.findOne({
        where: query
      })     

      if (!user) {
        throw {
          code: 404,
          name: 'UserNotFound'
        }
      }

      this.validateAccessToSingle({
        data: user,
        credentials,
      })

      await this.usersRepository.save({
        ...user,
        password,
        changePasswordAt: new Date(),
      })

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findManyUsers({ query, credentials }: FindManyUsersService): Promise<FindManyUsers> {
    try {
      const { limit = '10', offset, sortBy = 'signUpAt'} = query

      const { findAllQuery, findingQuery, hasPage, sortingCondition } = buildFindingQuery({
        query: {
          ...query,
          sortBy
        }
      })

      const promises = []

      if (hasPage) {
        promises.push(
          this.usersRepository.find({
            where: findingQuery,
            take: Number(limit),
            skip: Number(offset),
            order: sortingCondition
          }),
          this.usersRepository.count({
            where: findAllQuery
          })
        )
      }

      if (!hasPage) {
        promises.push(
          this.usersRepository.find({
            where: findAllQuery,
            order: sortingCondition,
          }),
          this.usersRepository.count({
            where: findAllQuery
          })
        )
      }

      const [users, totalCount] = await Promise.all(promises)

      users.forEach(each => {
        delete each.password
      })

      const { validData } = this.validateAccessToList<Users>({
        data: users,
        credentials,
      })

      const nextCursor = getNextCursor({
        data: users,
        sortBy,
      })

      return {
        totalCount,
        list: validData,
        cursor: nextCursor
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne({ query, credentials }: DeleteOneService): Promise<boolean> {
    try {
      const user = await this.usersRepository.find({
        where: query
      })

      if (!user) {
        throw {
          code: 404,
          name: 'UserNotFound'
        }
      }

      this.validateAccessToSingle({
        data: user,
        credentials,
      })

      await this.usersRepository.remove(user)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }
}