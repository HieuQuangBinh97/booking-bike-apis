import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigsModule } from '../../configs/configs.module'
import { AuthModule } from '../../middleware/auth/auth.module'
import { DriversModule } from '../drivers/drivers.module'
import { UserRolesModule } from '../user_roles/user_roles.module'
import { UserCombinedService } from './combined.service'
import { Users } from './models/users.schema'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'

const UsersRepository = TypeOrmModule.forFeature([Users])

@Module({
  imports: [
    UsersRepository,
    ConfigsModule,
    forwardRef(() => AuthModule),
    UserRolesModule,
    forwardRef(() => DriversModule),
  ],
  providers: [
    UsersService,
    UserCombinedService,
  ],
  exports: [
    UsersService,
    UserCombinedService,
    UsersRepository,
  ],
  controllers: [
    UsersController,
  ]
})

export class UsersModule { }