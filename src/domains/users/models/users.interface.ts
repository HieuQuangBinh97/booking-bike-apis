import { Credentials } from '../../../shared/validate_access'
import { Users } from './users.schema'

interface CreateOneData {
  firstName: string
  lastName?: string
  email?: string
  phoneNumber: string
  address?: string
  username?: string
  password: string
  avatar?: string
  identityCardNumber?: number
  frontOfIdentityCard?: string
  backOfIdentityCard?: string
  status: string
  signUpAt?: Date
  facebookID?: string
  googleID?: string
}

interface FindOneQuery {
  userID?: string
  email?: string
  phoneNumber?: string
  username?: string
  facebookID?: string
  googleID?: string
}

interface UpdateData {
  firstName?: string
  lastName?: string
  email?: string
  phoneNumber?: string
  address?: string
  avatar?: string
  identityCardNumber?: number
  frontOfIdentityCard?: string
  backOfIdentityCard?: string
  status?: string
  signUpAt?: Date
  facebookID?: string
  googleID?: string
}

interface FindManyQuery {
  limit?: string
  offset?: string
  sortBy?: string
  sortDirection?: string
  cursor?: string
  firstName?: string
  lastName?: string
  email?: string
  phoneNumber?: string
  identityCardNumber?: number
  status?: string
  signUpAt?: Date
  facebookID?: string
  googleID?: string
}

interface CheckExistUser {
  userID?: string
  email?: string
  phoneNumber?: string 
}

export interface CreateOneService {
  createOneData: CreateOneData
  credentials: Credentials
}

export interface FindOneService {
  query: FindOneQuery,
  credentials: Credentials,
  getPassword?: boolean
}

export interface UpdateOneService {
  query: FindOneQuery
  updateData: UpdateData
  credentials: Credentials
}

export interface ChangePasswordService {
  query: FindOneQuery
  password: string
  credentials: Credentials
}

export interface CheckExistUserService {
  query: CheckExistUser
  credentials: Credentials
}

export interface SignUpResponse {
  accessToken: string
  userData: Users
}

export interface FindManyUsersService {
  query: FindManyQuery
  credentials: Credentials
}

export interface FindManyUsers {
  totalCount: number
  list: Users[]
  cursor: string
}

export interface DeleteOneService {
  query: FindOneQuery
  credentials: Credentials
}