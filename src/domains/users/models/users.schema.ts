import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { } from '@nestjs/typeorm'

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  userID: string

  @Column({ type: 'varchar', nullable: false })
  firstName: string

  @Column({ type: 'varchar', nullable: true })
  lastName: string

  @Column({ type: 'varchar', nullable: true })
  email: string

  @Column({ type: 'varchar', nullable: false })
  phoneNumber: string

  @Column({ type: 'text', nullable: true })
  address: string

  @Column({ type: 'varchar', nullable: true })
  username: string

  @Column({ type: 'varchar', nullable: false })
  password: string

  @Column({ type: "datetime" })
  lastLoggedAt: Date

  @Column({ type: 'varchar', nullable: true })
  avatar: string

  @Column({ type: 'int', nullable: true })
  identityCardNumber: number

  @Column({ type: 'varchar', nullable: true })
  frontOfIdentityCard: string

  @Column({ type: 'varchar', nullable: true })
  backOfIdentityCard: string

  @Column({ type: 'varchar', nullable: false })
  status: string

  @CreateDateColumn({ type: 'datetime' })
  signUpAt: Date

  @Column({ type: 'datetime', default: null })
  changePasswordAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date

  @Column({ type: 'varchar', nullable: true })
  facebookID: string

  @Column({ type: 'varchar', nullable: true })
  googleID: string
}