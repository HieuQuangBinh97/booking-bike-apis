import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmail, IsEnum, IsNumber, IsNumberString, IsOptional, IsString } from "class-validator";

const sortBy = ['userID', 'createdAt', 'lastLoggedAt', 'email', 'signUpAt', 'identityCardNumber']

export class SignUpDto {
  @ApiProperty()
  @IsString()
  readonly firstName: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly lastName?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  @IsEmail()
  readonly email?: string

  @ApiProperty()
  @IsString()
  readonly phoneNumber: string

  @ApiProperty()
  @IsString()
  readonly password: string

  @ApiProperty()
  @IsString()
  readonly confirmPassword: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly avatar?: string
}

export class LoginDataDto {
  @ApiProperty()
  @IsString()
  readonly phoneNumber: string

  @ApiProperty()
  @IsString()
  readonly password: string
}

export class UpdateProfileDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly firstName?: string
  
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly lastName?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly email?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly address?: string
  
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly avatar?: string
}

export class ChangePasswordDto {
  @ApiProperty()
  @IsString()
  readonly oldPassword: string

  @ApiProperty()
  @IsString()
  readonly password: string

  @ApiProperty()
  @IsString()
  readonly confirmPassword: string
}

export class UpdateContactDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly userName?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsEmail()
  @IsString()
  readonly email?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly phoneNumber?: string
}

export class FindManyUsersDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsNumberString()
  readonly limit?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumberString()
  readonly offset?: string

  @ApiPropertyOptional({ enum: sortBy })
  @IsOptional()
  @IsEnum(sortBy)
  readonly sortBy?: string

  @ApiPropertyOptional({ enum: ['ASC', 'DESC'] })
  @IsOptional()
  @IsEnum(['ASC', 'DESC'])
  readonly sortDirection?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly cursor?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly email?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly phoneNumber?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly facebookID?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly googleID?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly firstName?: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  readonly lastName?: string
}