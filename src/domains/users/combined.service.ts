import { Injectable } from '@nestjs/common'
import { DriversService } from '../drivers/drivers.service'
import { UserRolesService } from '../user_roles/user_roles.service'
import { UsersService } from './users.service'

@Injectable()
export class UserCombinedService {
  constructor(
    private readonly usersService: UsersService,
    private readonly userRolesService: UserRolesService,
    private readonly driversService: DriversService,
  ) {}

  async getUserCredentials({ userID, credentials }): Promise<any> {
    try {
      const user = await this.usersService.findOne({
        query: { userID },
        credentials,
      })

      if (!user) {
        throw {
          code: 404,
          name: 'UserNotFound'
        }
      }

      const [userRoles, drivers] = await Promise.all([
        this.userRolesService.findMany({
          query: { userID },
          credentials,
        }),
        this.driversService.findMany({
          query: { userID },
          credentials,
        })
      ]) 

      let access = []

      if (userRoles && userRoles.list && userRoles.list.length) {
        access = userRoles.list.map(each => {
          return {
            roleName: each.roleName,
            status: each.status
          }
        })
      }

      let driverIDs = []

      if (drivers && drivers.list && drivers.list.length) {
        driverIDs = drivers.list.map(each => each.driverID)
      }

      return {
        ...user,
        access,
        driverIDs,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  } 
}
