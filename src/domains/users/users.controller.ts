import { BadRequestException, Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Query, Request, UseGuards, UsePipes } from '@nestjs/common'
import { ValidationPipe } from '../../middleware/pipe/validation.pipe'
import { AuthGuard } from '@nestjs/passport'
import { scopes } from '../../constants/scopes'
import { Scopes } from '../../middleware/authz/authz.service'
import { Users } from './models/users.schema'
import { UsersService } from './users.service'
import { SignUpDto, LoginDataDto, UpdateProfileDto, ChangePasswordDto, UpdateContactDto, FindManyUsersDto } from './models/users.dto'
import * as bcrypt from 'bcrypt' 
import { ConfigsService } from '../../configs/configs.service'
import { AccessToken, AuthService } from '../../middleware/auth/auth.service'
import { UserRolesService } from '../user_roles/user_roles.service'
import { FindManyUsers, SignUpResponse } from './models/users.interface'
import { checkControllerErrors } from '../../helpers/check_error'

@Controller()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly configsService: ConfigsService,
    private readonly authService: AuthService,
    private readonly userRolesService: UserRolesService,
  ) {}

  @Post('auth/signUp')
  @UsePipes(new ValidationPipe())
  async signUp(
    @Body() signUpDto: SignUpDto,
  ): Promise<SignUpResponse> {
    try {      
      const { password, confirmPassword, phoneNumber, ...createOneData } = signUpDto

      const [existUserEmail, existUserPhoneNumber] = await Promise.all([
        this.usersService.checkExistUser({
        query: { email: signUpDto.email },
        credentials: { isAdmin: true }
        }),
        this.usersService.checkExistUser({
          query: { phoneNumber },
          credentials: { isAdmin: true }
        })
      ])

      if (existUserEmail) {
        throw {
          code: 400,
          name: 'EmailIsAlreadyExist',
        }
      }

      if (existUserPhoneNumber) {
        throw {
          code: 400,
          name: 'PhoneNumberIsAlreadyExist',
        }
      }

      if (password !== confirmPassword) {
        throw {
          code: 400,
          name: 'IncorrectConfirmPassword'
        }
      }

      const hasPass = await bcrypt.hash(password, this.configsService.bcryptSalt)


      const newUser = await this.usersService.createOne({
        createOneData: {
          ...createOneData,
          phoneNumber,
          password: hasPass,
          status: 'ACTIVE',
        },
        credentials: { isAdmin: true}
      })

      await this.userRolesService.createOne({
        createOneData: {
          userID: newUser.userID,
          roleName: 'BASIC',
          status: 'ACTIVE',
        },
        credentials: { isAdmin: true }
      })

      const jwtToken = await this.authService.generateJWT(newUser)


      return {
        accessToken: jwtToken.accessToken,
        userData: newUser
      }
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Post('auth/login/password')
  @UsePipes(new ValidationPipe())
  async loginPassword(
    @Body() loginDataDto: LoginDataDto,
  ): Promise<AccessToken> {
    try {
      const { phoneNumber, password } = loginDataDto

      const existUser = await this.usersService.findOne({
        query: { phoneNumber },
        credentials: { isAdmin: true },
        getPassword: true,
      })

      if (!existUser) {
        throw {
          code: 400,
          name: 'UserNotFound'
        }
      }

      const isRightPassword = await bcrypt.compare(password, existUser.password)

      if (!isRightPassword) {
        throw {
          code: 400,
          name: 'PasswordIsInCorrect',
        }
      }

      const jwtToken = await this.authService.generateJWT(existUser)

      return jwtToken
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('users/me')
  @UseGuards(new Scopes([[scopes.USERS_READ]]))
  @UseGuards(AuthGuard('jwt'))
  async getProfile(
    @Request() { user },
  ): Promise<Users> {
    try {
      const { userID } = user

      const userData = await this.usersService.findOne({
        query: { userID },
        credentials: user,
      })

      return userData
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('users/me')
  @UseGuards(new Scopes([[scopes.USERS_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async updateProfile(
    @Request() { user },
    @Body() updateProfile: UpdateProfileDto,
  ): Promise<boolean> {
    try {
      const { userID } = user

      const isUpdated = await this.usersService.updateOne({
        query: { userID },
        credentials: user,
        updateData: updateProfile,
      })

      return isUpdated
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('users/me/password')
  @UseGuards(new Scopes([[scopes.USERS_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async changePassword(
    @Request() { user },
    @Body() changePassword: ChangePasswordDto,
  ): Promise<boolean> {
    try {
      const { oldPassword, password, confirmPassword } = changePassword

      if (oldPassword === password) {
        throw {
          code: 400,
          name: "NewPasswordDoNotBeSampleOldPassword",
        }
      }

      const { userID } = user

      const userData = await this.usersService.findOne({
        query: { userID },
        credentials: user,
        getPassword: true
      })

      if (userData && userData.password) {
        const isCorrectOldPassword = await bcrypt.compare(oldPassword, userData.password)

        if (!isCorrectOldPassword) {
          throw {
            code: 400,
            name: 'OldPasswordWrong'
          }
        }
      }

      if (password !== confirmPassword) {
        throw {
          code: 400,
          name: 'ConfirmPasswordWasNotMatchNewPassword'
        }
      }

      const hasPassword = await bcrypt.hash(password, this.configsService.bcryptSalt)

      await this.usersService.changePassword({
        query: { userID },
        password: hasPassword,
        credentials: user,
      })

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }


  @Put('users/me/contact')
  @UseGuards(new Scopes([[scopes.USERS_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async updateContact(
    @Request() { user },
    @Body() updateContactData: UpdateContactDto,
  ): Promise<boolean> {
    try {
      const { email, phoneNumber } = updateContactData

      const [isExistEmail, isExistPhoneNumber] = await Promise.all([
        this.usersService.checkExistUser({
          query: { email },
          credentials: user,
        }),
        this.usersService.checkExistUser({
          query: { phoneNumber },
          credentials: user,
        })
      ])

      if (isExistEmail) {
        throw {
          code: 400,
          name: 'EmailIsAlreadyExist'
        }
      }

      if (isExistPhoneNumber) {
        throw {
          code: 400,
          name: 'PhoneNumberIsAlreadyExist'
        }
      }

      await this.usersService.updateOne({
        updateData: updateContactData,
        credentials: user,
        query: { userID: user.userID }
      })

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('users')
  @UseGuards(new Scopes([]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async getManyUser(
    @Request() { user },
    @Query() findManyQuery: FindManyUsersDto,
  ): Promise<FindManyUsers> {
    try {
      const users = await this.usersService.findManyUsers({
        query: findManyQuery,
        credentials: user,
      })

      return users
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Delete('users/:userID')
  @UseGuards(new Scopes([[scopes.USERS_DELETE]]))
  @UseGuards(AuthGuard('jwt'))
  async deleteOneUser(
    @Request() { user },
    @Param() { userID },
  ): Promise<boolean> {
    try {
      await this.usersService.deleteOne({
        query: { userID },
        credentials: user,
      })

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('users/:userID')
  @UseGuards(new Scopes([]))
  @UseGuards(AuthGuard('jwt'))
  async findOne(
    @Request() { user },
    @Param() { userID },
  ): Promise<Users> {
    try {
      const userData = await this.usersService.findOne({
        query: { userID },
        credentials: user,
      })

      return userData
    } catch (error) {
      checkControllerErrors(error)
    }
  }
}