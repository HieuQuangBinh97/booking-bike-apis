import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { validateAccessToList, validateAccessToSingle } from '../../shared/validate_access'
import { EntityRepository, Repository } from 'typeorm'
import { CreateOneService, FindManyService, FindOneService, ManyUserRoles } from './models/user_roles.interface'
import { UserRoles } from './models/user_roles.schema'
import { buildFindingQuery } from '../../helpers/build'
import { getNextCursor } from '../../helpers/gets'


@EntityRepository(UserRoles)
export class UserRolesService {
  constructor(
    @InjectRepository(UserRoles)
    private readonly userRolesRepository: Repository<UserRoles>,
  ) { }

  private readonly validateAccessToSingle = validateAccessToSingle
  private readonly validateAccessToList = validateAccessToList

  async createOne({ createOneData, credentials }: CreateOneService): Promise<UserRoles> {
    try {
      const createPayload = {
        ...createOneData,
        createdBy: credentials.userID
      }

      const userRole = await this.userRolesRepository.save(createPayload)

      return userRole
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne({ query, credentials }: FindOneService): Promise<UserRoles> {
    try {
      const userRole = await this.userRolesRepository.findOne({
        where: query,
        relations: ['userID', 'createdBy']
      })

      this.validateAccessToSingle({
        data: userRole,
        credentials,
      })

      return userRole
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany({ query, credentials }: FindManyService): Promise<ManyUserRoles> {
    try {
      const { limit, offset, sortBy = 'userRoleID'} = query

      const { findAllQuery, findingQuery, hasPage, sortingCondition } = buildFindingQuery({
        query: {
          ...query,
          sortBy
        }
      })

      const promises = []

      if (hasPage) {
        promises.push(
          this.userRolesRepository.find({
            where: findingQuery,
            take: Number(limit),
            skip: Number(offset),
            order: sortingCondition,
            relations: ['userID', 'createdBy']
          }),
          this.userRolesRepository.count({
            where: findAllQuery
          })
        )
      }

      if (!hasPage) {
        promises.push(
          this.userRolesRepository.find({
            where: findAllQuery,
            order: sortingCondition,
            relations: ['userID', 'createdBy']
          }),
          this.userRolesRepository.count({
            where: findAllQuery
          })
        )
      }

      const [userRoles, totalCount] = await Promise.all(promises)

      const { validData } = this.validateAccessToList<UserRoles>({
        data: userRoles,
        credentials,
      })

      const nextCursor = getNextCursor({
        data: userRoles,
        sortBy,
      })

      return {
        totalCount,
        list: validData,
        cursor: nextCursor,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }
}