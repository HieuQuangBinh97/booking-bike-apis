import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserRoles } from './models/user_roles.schema'
import { UserRolesService } from './user_roles.service'

const userRoleRepository = TypeOrmModule.forFeature([UserRoles])

@Module({
  imports: [
    userRoleRepository,
  ],
  exports: [
    UserRolesService,
    userRoleRepository
  ],
  providers: [
    UserRolesService
  ]
})

export class UserRolesModule { }