import { Users } from '../../users/models/users.schema'
import { Column, CreateDateColumn, Entity, JoinColumn,
  ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

@Entity()
export class UserRoles {
  @PrimaryGeneratedColumn('uuid')
  userRoleID: string

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userID' })
  userID: string

  @Column({ type: 'varchar', nullable: false })
  roleName: string

  @Column({ type: 'varchar', nullable: false })
  status: string

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'createdBy' })
  createdBy: string

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date
}