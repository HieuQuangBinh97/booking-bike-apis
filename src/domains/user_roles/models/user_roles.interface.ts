import { Credentials } from '../../../shared/validate_access'
import { UserRoles } from './user_roles.schema'

interface CreateOneData {
  userID: string
  roleName: string
  status: string
  createdBy?: string
}

interface FindOneQuery {
  userRoleID: string
  roleName?: string
  userID?: string
  createdBy?: string
}

interface FindManyQuery {
  userRoleID?: string
  roleName?: string
  userID?: string
  createdBy?: string
  status?: string
  limit?: string
  offset?: string
  cursor?: string
  sortBy?: string
  sortCondition?: string
}

export interface CreateOneService {
  createOneData: CreateOneData
  credentials: Credentials
}

export interface FindOneService {
  query: FindOneQuery
  credentials: Credentials
}

export interface FindManyService {
  query: FindManyQuery
  credentials: Credentials
}

export interface ManyUserRoles {
  totalCount: number
  list: UserRoles[]
  cursor: string
}