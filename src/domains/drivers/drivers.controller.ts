import { Body, Controller, Get, Param, Post, Put, Query, Request, UploadedFiles, UseGuards, UseInterceptors, UsePipes } from '@nestjs/common'
import { ValidationPipe } from '../../middleware/pipe/validation.pipe'
import { AuthGuard } from '@nestjs/passport';
import { scopes } from '../../constants/scopes';
import { DriversService } from './drivers.service';
import { ConfirmDriverDto, CreateOneDriverDto, FindDriverNearLocationDto, FindManyDriverDto, RateDriverDto, UpdateDriverLocationDto } from './models/drivers.dto';
import { Drivers } from './models/drivers.schema';
import { checkControllerErrors } from '../../helpers/check_error';
import { UsersService } from '../users/users.service';
import { Scopes } from '../../middleware/authz/authz.service';
import { FindManyDriver } from './models/drivers.interface';
import { UserRolesService } from '../user_roles/user_roles.service';
import { SocketGateway } from '../socket/socket.gateway';
import { FileFieldsInterceptor, FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { storageFile } from '../../middleware/helper/file';
import { VehicleBookingsService } from '../vehicle_bookings/vehicle_bookings.service';
import { NotificationsService } from '../notifications/notifications.service';
import { NotificationTypes } from '../notifications/models/notifications.schema';

@Controller()
export class DriversController {
  constructor(
    private readonly driversService: DriversService,
    private readonly usersService: UsersService,
    private readonly userRolesService: UserRolesService,
    private readonly socketGateway: SocketGateway,
    private readonly vehicleBookingsService: VehicleBookingsService,
    private readonly notificationsService: NotificationsService,
  ) {}

  @Post('drivers')
  @UseGuards(new Scopes([[scopes.DRIVER_CREATE], [scopes.USERS_READ]]))
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'frontOfIdentityCard', maxCount: 1 },
      { name: 'backOfIdentityCard', maxCount: 1 },
    ])
  )
  async createOneDriver(
    @Request() { user },
    @UploadedFiles() { frontOfIdentityCard, backOfIdentityCard },
    @Body() createOneDriver,
  ): Promise<Drivers> {
    try {
      const { userID, identityCardNumber } = createOneDriver
      
      let registerID = userID ? userID : user.userID

      const existDriver = await this.driversService.findMany({
        query: { userID: registerID },
        credentials: {
          ...user,
          isAdmin: true
        }
      })

      if (existDriver && existDriver.list && existDriver.list.length) {
        throw {
          code: 400,
          name: 'UserCanOnlyBecomeOneDriver'
        }
      }


      const currentUsers = await this.usersService.findOne({
        query: { userID: registerID },
        credentials: { ...user, isAdmin: true },
      })

      if (!identityCardNumber || !frontOfIdentityCard || !backOfIdentityCard) {
        throw {
          code: 400,
          name: 'MissingIdentityCardInfo'
        }
      }

      const pathToFrontOfIdentityCard = await storageFile({
        buffer: frontOfIdentityCard[0].buffer,
        fileName: `${registerID}_frontOfIdentityCard`,
        originalName: frontOfIdentityCard[0].originalname
      })

      const pathToBackOfIdentityCard = await storageFile({
        buffer: backOfIdentityCard[0].buffer,
        fileName: `${registerID}_backOfIdentityCard`,
        originalName: backOfIdentityCard[0].originalname
      })

      const newDriver = await this.driversService.createOne({
        createOneData: {
          status: 'processing',
          userID: registerID,
        },
        credentials: user,
      })

      await this.usersService.updateOne({
        query: { userID: registerID },
        updateData: {
          frontOfIdentityCard: pathToFrontOfIdentityCard,
          backOfIdentityCard: pathToBackOfIdentityCard,
          identityCardNumber,
        },
        credentials: user,
      })

      const topic = `user-${currentUsers.userID}`
      
      // send notification to admin to confirm driver

      return newDriver
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('drivers/:driverID/staff/confirm')
  @UseGuards(new Scopes([[scopes.DRIVER_CONFIRM], [scopes.DRIVER_UPDATE], [scopes.DRIVER_READ]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async confirmDriver(
    @Request() { user }, 
    @Body() confirmData: ConfirmDriverDto,
    @Param() { driverID }
  ): Promise<Drivers> {
    try {
      const { status, deniedReason } = confirmData

      const driver: any = await this.driversService.findOne({
        query: { driverID },
        credentials: { 
          ...user,
          isAdmin: true,
        },
      })

      if (driver.status === 'accepted' || driver.status === 'canceled') {
        throw {
          code: 400,
          name: 'DriverWasConfirmed'
        }
      }

      if (status === 'denied') {
        const deniedDriver: any = await this.driversService.updateOne({
          query: { driverID },
          credentials: {
            ...user,
            isAdmin: true,
          },
          updateData: {
            status,
          },
        })

        const newDeniedNotification = await this.notificationsService.createOne({
          createOneData: {
            title: 'Quản trị viên Từ chối yêu cầu của bạn',
            data: deniedReason ? `Yêu cầu của bạn bị từ chối vì ${deniedReason}` : 'Đã từ chối cho bạn trở thành tài xế',
            createdBy: user.userID,
            isRead: false,
            targetUserID: driver.userID.userID ? driver.userID.userID : driver.userID,
            driverID: driver.driverID,
            type: NotificationTypes.ADMIN_DENIED_DRIVER,
          },
          credentials: user,
        })
  
        const deniedTopic = `user-${deniedDriver.userID.userID ? deniedDriver.userID.userID : deniedDriver.userID }`
  
        this.socketGateway.server
        .to(deniedTopic)
        .emit(
          NotificationTypes.ADMIN_DENIED_DRIVER,
          {
            data: deniedDriver,
            notification: newDeniedNotification
          }
        )
  
        return deniedDriver 
      }


      const updatedDriver: any = await this.driversService.updateOne({
        query: { driverID },
        credentials: {
          ...user,
          isAdmin: true,
        },
        updateData: {
          status,
          workingStatus: 'free'
        },
      })

      

      await this.userRolesService.createOne({
        createOneData: {
          roleName: 'DRIVER',
          status: 'ACTIVE',
          userID: driver.userID && driver.userID.userID ? driver.userID.userID : driver.userID,
        },
        credentials: {
          ...user,
          isAdmin: true,
        }
      })

      const newNotification = await this.notificationsService.createOne({
        createOneData: {
          title: 'Quản trị viên',
          data: 'Đã phê duyệt để bạn trở thành tài xế',
          createdBy: user.userID,
          isRead: false,
          targetUserID: driver.userID.userID ? driver.userID.userID : driver.userID,
          driverID: driver.driverID,
          type: NotificationTypes.ADMIN_ACCEPTED_DRIVER,
        },
        credentials: user,
      })

      const topic = `user-${updatedDriver.userID.userID ? updatedDriver.userID.userID : updatedDriver.userID }`

      this.socketGateway.server
      .to(topic)
      .emit(
        NotificationTypes.ADMIN_ACCEPTED_DRIVER,
        {
          data: updatedDriver,
          notification: newNotification
        }
      )

      return updatedDriver 
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('drivers/nearLocation')
  @UseGuards(new Scopes([]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async findDriverNearLocation(
    @Request() { user },
    @Query() findNearLocationQuery: FindDriverNearLocationDto,
  ): Promise<FindManyDriver> {
    try {
      const drivers = await this.driversService.findAvailableDriverNearLocation({
        query: findNearLocationQuery,
        credentials: user,
      })

      return drivers
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('drivers/:driverID')
  @UseGuards(new Scopes([[scopes.DRIVER_READ]]))
  @UseGuards(AuthGuard('jwt'))
  async findOneDriver(
    @Request() { user },
    @Param() { driverID },
  ):Promise<Drivers> {
    try {
      const driver = await this.driversService.findOne({
        query: { driverID },
        credentials: user,
      })

      return driver
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('drivers')
  @UseGuards(new Scopes([[scopes.DRIVER_READ]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async findManyDriver(
    @Request() { user },
    @Query() findManyQuery: FindManyDriverDto,
  ): Promise<FindManyDriver> {
    try {
      const drivers = await this.driversService.findMany({
        query: findManyQuery,
        credentials: user,
      })

      return drivers
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('drivers/:driverID/updateLocation')
  @UseGuards(new Scopes([[scopes.DRIVER_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async updateDriverLocation(
    @Request() { user },
    @Param() { driverID },
    @Body() updateLocationData: UpdateDriverLocationDto,
  ): Promise<boolean> {
    try {
      await this.driversService.updateOne({
        query: { driverID },
        credentials: user,
        updateData: updateLocationData,
      })

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('drivers/:driverID/rate')
  @UseGuards(new Scopes([]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async rateDriver(
    @Request() { user },
    @Param() { driverID },
    @Body() rateDriverData: RateDriverDto,
  ): Promise<boolean> {
    try {
      const { rating: ratingDto } = rateDriverData

      const [driver, currentUsers]: any = await Promise.all([
        this.driversService.findOne({
          query: { driverID },
          credentials: {
            ...user,
            isPublic: true,
          }
        }),
        this.usersService.findOne({
          query: { userID: user.userID },
          credentials: user,
        })
      ])

      const { rating, rateTimes } = driver

      const updateRate = ( rating * rateTimes + ratingDto ) / ( rateTimes + 1)

      await this.driversService.updateOne({
        query: { driverID },
        credentials: {
          ...user,
          isPublic: true,
        },
        updateData: {
          rating: updateRate,
          rateTimes: rateTimes + 1
        }
      })

      const newNotification = await this.notificationsService.createOne({
        createOneData: {
          title: `${currentUsers.firstName} ${currentUsers.lastName}`,
          data: `Đã đánh giá bạn ${rating} sao`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: driver.userID.userID ? driver.userID.userID : driver.userID,
          driverID: driver.driverID,
          type: NotificationTypes.RATING_DRIVER,
        },
        credentials: user,
      })

      const topic = `user-${driver.userID.userID ? driver.userID.userID : driver.userID }`

      this.socketGateway.server
      .to(topic)
      .emit(
        NotificationTypes.RATING_DRIVER,
        {
          data: driver,
          notification: newNotification
        }
      )

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('drivers/me/statistic')
  @UseGuards(new Scopes([[scopes.DRIVER_READ]]))
  @UseGuards(AuthGuard('jwt'))
  async getDriverOwnerStatistic(
    @Request() { user },
  ): Promise<any> {
    try {
      const driver: any = await this.driversService.findOne({
        query: { userID: user.userID },
        credentials: user,
      })

      const vehicleBookings = await this.vehicleBookingsService.findMany({
        query: { driverID: driver.driverID , sortBy: 'createdAt', sortDirection: 'DESC', limit: '15'},
        credentials: user,
      })

      return {
        driver,
        vehicleBookings,
      }
    } catch (error) {
      checkControllerErrors(error)
    }
  }
}