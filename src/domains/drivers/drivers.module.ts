import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { NotificationsModule } from '../notifications/notifications.module'
import { SocketModule } from '../socket/socket.module'
import { UsersModule } from '../users/users.module'
import { UserRolesModule } from '../user_roles/user_roles.module'
import { VehicleBookingsModule } from '../vehicle_bookings/vehicle_bookings.module'
import { DriversController } from './drivers.controller'
import { DriversService } from './drivers.service'
import { Drivers } from './models/drivers.schema'

const driversRepository = TypeOrmModule.forFeature([Drivers])


@Module({
  imports: [
    forwardRef(() => SocketModule),
    driversRepository,
    UserRolesModule,
    forwardRef(() => VehicleBookingsModule),
    forwardRef(() => UsersModule),
    NotificationsModule,
  ],
  exports: [
    DriversService,
  ],
  providers: [
    DriversService,
  ],
  controllers: [
    DriversController,
  ],
})

export class DriversModule { }
