import { InjectRepository } from '@nestjs/typeorm'
import { validateAccessToList, validateAccessToSingle } from '../../shared/validate_access'
import { Between, EntityRepository, Repository } from 'typeorm'
import { Drivers } from './models/drivers.schema'
import { CreateOneDriverService, DeleteOneService,
  FindAvailableDriverNearLocation,
  FindManyDriver,
  FindManyService, FindOneService, UpdateOneService } from './models/drivers.interface'
import { buildFindingQuery } from '../../helpers/build'
import { getNextCursor } from '../../helpers/gets'

@EntityRepository(Drivers)
export class DriversService {
  constructor(
    @InjectRepository(Drivers)
    private readonly driversRepository: Repository<Drivers>,
  ) { }

  private readonly validateAccessToSingle = validateAccessToSingle
  private readonly validateAccessToList = validateAccessToList

  async createOne({ createOneData, credentials }: CreateOneDriverService): Promise<Drivers> {
    try {
      const payload = {
        ...createOneData,
        createdBy: credentials.userID
      }

      this.validateAccessToSingle({
        data: payload,
        credentials,
      })

      const newDriver = await this.driversRepository.save(payload)

      return newDriver
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne({ query, credentials}: FindOneService): Promise<Drivers> {
    try {
      const driver = await this.driversRepository.findOne({
        where: query,
        relations: ['userID', 'createdBy']
      })

      if (!driver) {
        throw {
          code: 404,
          name: 'DriverNotFound'
        }
      }

      this.validateAccessToSingle({
        data: driver,
        credentials,
      })

      return driver
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ query, credentials, updateData }: UpdateOneService): Promise<Drivers> {
    try {
      const driver = await this.driversRepository.findOne({
        where: query,
        relations: ['userID', 'createdBy'],
      })

      if (!driver) {
        throw {
          code: 404,
          name: 'DriverNotFound'
        }
      }

      this.validateAccessToSingle({
        data: driver,
        credentials,
      })

      const updatedDriver = await this.driversRepository.save({
        ...driver,
        ...updateData,
        updatedAt: new Date(),
      })

      return updatedDriver
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany({ query, credentials}: FindManyService): Promise<FindManyDriver> {
    try {
      const { sortBy = 'createdAt', limit, offset} = query

      const { findAllQuery, findingQuery, hasPage, sortingCondition } = buildFindingQuery({
        query: {
          ...query,
          sortBy,
        }
      })

      const promises = []

      if (hasPage) {
        promises.push(
          this.driversRepository.find({
            where: findingQuery,
            relations: ['userID', 'createdBy'],
            take: Number(limit),
            skip: Number(offset),
            order: sortingCondition
          }),
          this.driversRepository.count({
            where: findAllQuery
          })
        )
      }

      if (!hasPage) {
        promises.push(
          this.driversRepository.find({
            where: findAllQuery,
            relations: ['userID', 'createdBy'],
            order: sortingCondition
          }),
          this.driversRepository.count({
            where: findAllQuery
          })
        )
      }

      const [drivers, totalCount] = await Promise.all(promises)

      const { validData } = this.validateAccessToList<Drivers>({
        data: drivers,
        credentials,
      })

      const nextCursor = getNextCursor({
        data: drivers,
        sortBy,
      })

      return {
        totalCount,
        list: validData,
        cursor: nextCursor
      }


    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne({ query, credentials }: DeleteOneService): Promise<boolean> {
    try {
      const driver = await this.driversRepository.findOne({
        where: query,
        relations: ['userID', 'createdBy']
      })

      if (!driver) {
        throw {
          code: 404,
          name: 'DriverNotFound'
        }
      }

      this.validateAccessToSingle({
        data: driver,
        credentials,
      })

      await this.driversRepository.remove(driver)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findAvailableDriverNearLocation({ query, credentials }: FindAvailableDriverNearLocation): Promise<FindManyDriver> {
    try {
      const { distance, lat, lng, limit, offset, sortBy = 'rating',...newQuery } = query

      const { findAllQuery, findingQuery, hasPage, sortingCondition } = buildFindingQuery({
        query: {
          ...newQuery,
          sortBy,
          lastLat: Between(lat - distance, lat + distance),
          lastLng: Between(lng - distance, lng + distance),
          status: 'accepted',
          workingStatus: 'free',
        }
      })

      const promises = []

      if (hasPage) {
        promises.push(
          this.driversRepository.find({
            where: findingQuery,
            relations: ['userID', 'createdBy'],
            take: Number(limit),
            skip: Number(offset),
            order: sortingCondition
          }),
          this.driversRepository.count({
            where: findAllQuery
          })
        )
      }

      if (!hasPage) {
        promises.push(
          this.driversRepository.find({
            where: findAllQuery,
            relations: ['userID', 'createdBy'],
            order: sortingCondition
          }),
          this.driversRepository.count({
            where: findAllQuery
          })
        )
      }

      const [drivers, totalCount] = await Promise.all(promises)

      const { validData } = this.validateAccessToList<Drivers>({
        data: drivers,
        credentials,
      })

      const nextCursor = getNextCursor({
        data: drivers,
        sortBy,
      })

      return {
        totalCount,
        list: validData,
        cursor: nextCursor
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }
}