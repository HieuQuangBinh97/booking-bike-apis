import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { Type } from "class-transformer"
import { IsEnum, IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString, Max, Min } from "class-validator"

export class CreateOneDriverDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly userID?: string

  @ApiPropertyOptional()
  @IsNumber()
  readonly identityCardNumber: number

  @ApiPropertyOptional()
  @IsNotEmpty()
  readonly frontOfIdentityCard: string

  @ApiPropertyOptional()
  @IsNotEmpty()
  readonly backOfIdentityCard: string
}

export class FindManyDriverDto {
  @ApiPropertyOptional()
  @IsNumberString()
  @IsOptional()
  readonly limit?: string

  @ApiPropertyOptional()
  @IsNumberString()
  @IsOptional()
  readonly offset?: string

  @ApiPropertyOptional({ enum: ['createdAt', 'rating', 'status', 'rateTimes']})
  @IsEnum(['createdAt', 'rating', 'status', 'rateTimes'])
  @IsOptional()
  readonly sortBy?: string

  @ApiPropertyOptional({ enum: ['ASC', 'DESC']})
  @IsEnum(['ASC', 'DESC'])
  @IsOptional()
  readonly sortDirection?: string = 'ASC'

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  cursor?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  userID?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  driverID?: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  createdBy?: string
  
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  status?: string

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @Min(0)
  @Max(5)
  rating?: number

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @Min(0)
  rateTimes?: number
}

export class ConfirmDriverDto {
  @ApiProperty({ enum: ['accepted', 'denied']})
  @IsEnum(['accepted', 'denied'])
  readonly status: string

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly deniedReason?: string
}

export class UpdateDriverLocationDto {
  @ApiProperty()
  @IsNumber()
  readonly lastLat: number

  @ApiProperty()
  @IsNumber()
  readonly lastLng: number
}

export class RateDriverDto {
  @ApiProperty()
  @IsNumber()
  @Min(0)
  @Max(5)
  readonly rating: number
}

export class FindDriverNearLocationDto {
  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  readonly distance: number

  @ApiProperty()
  @Type(() => Number)
  @IsNumber()
  readonly lat: number

  @ApiProperty()
  @Type(() => Number)
  @IsNumber()
  readonly lng: number
  
  @ApiPropertyOptional()
  @IsNumberString()
  @IsOptional()
  readonly limit?: string

  @ApiPropertyOptional()
  @IsNumberString()
  @IsOptional()
  readonly offset?: string

  @ApiPropertyOptional({ enum: ['createdAt', 'rating', 'status', 'rateTimes']})
  @IsEnum(['createdAt', 'rating', 'status', 'rateTimes'])
  @IsOptional()
  readonly sortBy?: string

  @ApiPropertyOptional({ enum: ['ASC', 'DESC']})
  @IsEnum(['ASC', 'DESC'])
  @IsOptional()
  readonly sortDirection?: string = 'ASC'

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  cursor?: string

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  readonly rating?: number
}