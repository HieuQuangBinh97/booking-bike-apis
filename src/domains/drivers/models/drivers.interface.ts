import { Credentials } from '../../../shared/validate_access'
import { Drivers } from './drivers.schema'

interface CreateOneData {
  userID: string
  status: string
  lastLat?: number
  lastLng?: number
  createdBy?: string
}

interface FindOneQuery {
  driverID?: string
  userID?: string
  createdBy?: string
  status?: string
  lastLat?: number
  lastLng?: number
}

interface UpdateOneData {
  status?: string
  lastLat?: number
  lastLng?: number
  vehicleID?: string[]
  lastLoggedAt?: Date
  updatedAt?: Date
  rating?: number
  rateTimes?: number
  workingStatus?: string
}

interface FindManyQuery {
  limit?: string
  offset?: string
  sortBy?: string
  sortDirection?: string
  cursor?: string
  userID?: string
  driverID?: string
  createdBy?: string
  status?: string
  rating?: number
  rateTimes?: number
}

export interface CreateOneDriverService {
  createOneData: CreateOneData
  credentials: Credentials
}

export interface FindOneService {
  query: FindOneQuery
  credentials: Credentials
}

export interface UpdateOneService {
  query: FindOneQuery
  updateData: UpdateOneData
  credentials: Credentials
}

export interface FindManyService {
  query: FindManyQuery
  credentials: Credentials
}

export interface DeleteOneService {
  query: FindOneQuery
  credentials: Credentials
}

export interface FindManyDriver {
  totalCount: number
  list: Drivers[]
  cursor: string
}

interface FindDriverNearLocationQuery {
  distance: number
  lat: number
  lng: number
  limit?: string
  offset?: string
  sortBy?: string
  sortDirection?: string
  cursor?: string
  rating?: number
}

export interface FindAvailableDriverNearLocation {
  query: FindDriverNearLocationQuery
  credentials: Credentials
}

