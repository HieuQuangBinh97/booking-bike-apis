import { Users } from '../../users/models/users.schema'
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export enum DriverType {
  DRIVER_MOTORBIKE = 'DRIVER_MOTORBIKE',
  DRIVER_CAR = 'DRIVER_CAR',
}

@Entity()
export class Drivers {
  @PrimaryGeneratedColumn('uuid')
  driverID: string

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userD' })
  userID: string

  @Column({ type: 'simple-array', nullable: true })
  vehicleID: string[]
  
  @Column({ type: 'varchar', nullable: false })
  status: string

  @Column({ type: 'varchar', default: null})
  workingStatus: string

  @Column({ type: 'varchar', nullable: false, default: 'DRIVER_MOTORCYCLE' })
  driverType: string

  @Column({ type: 'float', nullable: true })
  lastLat: number

  @Column({ type: 'float', nullable: true })
  lastLng: number

  @Column({ type: 'datetime', nullable: true })
  lastLoggedAt: Date

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'createdBy' })
  createdBy: string

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date

  @Column({ type: 'float', default: 0 })
  rating: number

  @Column({ type: 'int', default: 0 })
  rateTimes: number
}