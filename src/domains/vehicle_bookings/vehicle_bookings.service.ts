import { InjectRepository } from '@nestjs/typeorm'
import { validateAccessToList, validateAccessToSingle } from '../../shared/validate_access'
import { EntityRepository, Repository } from 'typeorm'
import { Status, VehicleBookings } from './models/vehicle_bookings.schema'
import { CreateOneService, DeleteOneService, FindManyService, FindManyVehicleBookings, FindOneService, UpdateOneService } from './models/vehicle_bookings.interface'
import { buildFindingQuery } from '../../helpers/build'
import { getNextCursor } from '../../helpers/gets'

@EntityRepository(VehicleBookings)
export class VehicleBookingsService {
  constructor(
    @InjectRepository(VehicleBookings)
    private readonly vehicleBookingRepository: Repository<VehicleBookings>
  ) { }

  private readonly validateAccessToSingle = validateAccessToSingle
  private readonly validateAccessToList = validateAccessToList

  async createOne({ createOneData, credentials }: CreateOneService): Promise<VehicleBookings> {
    try {
      const payload = {
        ...createOneData,
        createdBy: credentials.userID
      }

      this.validateAccessToSingle({
        data: payload,
        credentials,
      })

      const newVehicleBooking = await this.vehicleBookingRepository.save(payload)

      return newVehicleBooking
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ query, updateOneData, credentials }: UpdateOneService): Promise<VehicleBookings> {
    try {
      const vehicleBooking = await this.vehicleBookingRepository.findOne({
        where: query,
        relations: ['driverID', 'customerID', 'createdBy']
      })

      if (!vehicleBooking) {
        throw {
          code: 404,
          name: 'VehicleBookingNotFound'
        }
      }

      if (vehicleBooking.status === Status.CANCELED) {
        throw {
          code: 400,
          name: 'VehicleBookingWasCanceled'
        }
      }

      this.validateAccessToSingle({
        data: vehicleBooking,
        credentials,
      })

      const updatedVehicleBooking = await this.vehicleBookingRepository.save({
        ...vehicleBooking,
        ...updateOneData,
        updatedAt: new Date(),
      })

      return updatedVehicleBooking
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne({ query, credentials }: DeleteOneService): Promise<boolean> {
    try {
      const vehicleBooking = await this.vehicleBookingRepository.findOne({
        where: query,
        relations: ['driverID', 'customerID', 'createdBy']
      })

      if (!vehicleBooking) {
        throw {
          code: 404,
          name: 'VehicleBookingNotFound'
        }
      }

      this.validateAccessToSingle({
        data: vehicleBooking,
        credentials,
      })

      await this.vehicleBookingRepository.remove(vehicleBooking)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne({ query, credentials }: FindOneService): Promise<VehicleBookings> {
    try {
      const vehicleBooking = await this.vehicleBookingRepository.findOne({
        where: query,
        relations: ['driverID', 'customerID', 'createdBy']
      })

      if (!vehicleBooking) {
        throw {
          code: 404,
          name: 'VehicleBookingNotFound'
        }
      }

      this.validateAccessToSingle({
        data: vehicleBooking,
        credentials,
      })
      
      return vehicleBooking
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany({ query , credentials }: FindManyService): Promise<FindManyVehicleBookings> {
    try {
      const { sortBy = 'createdBy', limit, offset } = query

      const { findingQuery, findAllQuery, hasPage, sortingCondition } = buildFindingQuery({
        query: {
          ...query,
          sortBy,
        }
      })

      const promises = []

      if (hasPage) {
        promises.push(
          this.vehicleBookingRepository.find({
            where: findingQuery,
            relations: ['driverID', 'customerID', 'createdBy'],
            take: Number(limit),
            skip: Number(offset),
            order: sortingCondition
          }),
          this.vehicleBookingRepository.count({
            where: findAllQuery
          })
        )
      }

      if (!hasPage) {
        promises.push(
          this.vehicleBookingRepository.find({
            where: findAllQuery,
            relations: ['driverID', 'customerID', 'createdBy'],
            order: sortingCondition
          }),
          this.vehicleBookingRepository.count({
            where: findAllQuery
          })
        )
      }

      const [vehicleBookings, totalCount] = await Promise.all(promises)

      const { validData } = this.validateAccessToList<VehicleBookings>({
        data: vehicleBookings,
        credentials,
      })

      const nextCursor = getNextCursor({
        data: vehicleBookings,
        sortBy,
      })

      return {
        totalCount,
        list: validData,
        cursor: nextCursor
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async statistic() {
    try {
      const successQuery = `
        SELECT COUNT(vehicleBookingID) AS count,
        DATE(createdAt) as date
        FROM vehicle_bookings
        WHERE status = "FINISHED"
        GROUP BY date
        ORDER BY createdAt
      `

      const totalQuery = `
        SELECT COUNT(vehicleBookingID) AS count,
        DATE(createdAt) as date
        FROM vehicle_bookings
        GROUP BY date
        ORDER BY createdAt
      `

      const successData = await this.vehicleBookingRepository.query(successQuery)
      const totalData = await this.vehicleBookingRepository.query(totalQuery)

      return {
        successData,
        totalData,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  } 
}