import { Body, Controller, Get, Param, Post, Put, Request, Scope, UseGuards, UsePipes } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { scopes } from '../../constants/scopes'
import { VehicleBookingsService } from './vehicle_bookings.service'
import { ValidationPipe } from '../../middleware/pipe/validation.pipe'
import { Status, VehicleBookings } from './models/vehicle_bookings.schema'
import { checkControllerErrors } from '../../helpers/check_error'
import { Scopes } from '../../middleware/authz/authz.service'
import { BookingVehicleDto, DriverConfirmDto, SelectDriverDto, UpdateBookingDto } from './models/vehicle_bookings.dto'
import { DriversService } from '../drivers/drivers.service'
import { SocketGateway } from '../socket/socket.gateway'
import { NotificationTypes } from '../notifications/models/notifications.schema'
import { NotificationsService } from '../notifications/notifications.service'
import { UsersService } from '../users/users.service'
import { query } from 'express'
import { credential } from 'firebase-admin'

@Controller()
export class VehicleBookingsController {
  constructor(
    private readonly vehicleBookingsService: VehicleBookingsService,
    private readonly driversService: DriversService,
    private readonly socketGateway: SocketGateway,
    private readonly notificationsService: NotificationsService,
    private readonly usersService: UsersService
  ) { }

  @Post('vehicleBookings')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_CREATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async createVehicleBooking(
    @Request() { user },
    @Body() bookingData: BookingVehicleDto,
  ): Promise<object> {
    try {
      const { customerID, startingPoint } = bookingData

      const unfinishedBookingVehicle = await this.vehicleBookingsService.findMany({
        query: {
          customerID: customerID ? customerID : user.userID,
          status: [Status.PENDING, Status.DRIVER_ACCEPTED, Status.DRIVER_IS_COMING, Status.PROCESSING],
        },
        credentials: user,
      })

      if (unfinishedBookingVehicle && unfinishedBookingVehicle.list && unfinishedBookingVehicle.list.length) {
        throw {
          code: 400,
          name: 'UserCanNotCreateNewBooking'
        }
      }

      const newBooking = await this.vehicleBookingsService.createOne({
        createOneData: {
          ...bookingData,
          customerID: customerID ? customerID : user.userID,
          status: Status.PENDING,
        },
        credentials: user,
      })

      
      const driverNearLocations = await this.driversService.findAvailableDriverNearLocation({
        query: {
          distance: 100,
          lat: startingPoint.lat,
          lng: startingPoint.lng,
          limit: '20',
        },
        credentials: { isPublic: true }
      })

      return {
        vehicleBooking: newBooking,
        driverNearLocations,
      }
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('vehicleBookings/:vehicleBookingID/selectDriver')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async selectDriver(
    @Request() { user },
    @Param() { vehicleBookingID },
    @Body() selectDriver: SelectDriverDto,
  ): Promise<VehicleBookings> {
    try {
      const { driverID } = selectDriver

      const [driver, currentUser] = await Promise.all([
        this.driversService.findOne({
          query: { driverID },
          credentials: { isPublic: true },
        }),
        this.usersService.findOne({
          query: { userID: user.userID },
          credentials: user,
        })
      ])

      if (!driver) {
        throw {
          code: 404,
          name: 'DriverNotFound',
        }
      }

      const updatedBooking = await this.vehicleBookingsService.updateOne({
        updateOneData: {
          driverID,
          status: Status.PROCESSING,
        },
        credentials: user,
        query: { vehicleBookingID }
      })

      const targetID = driver.userID && driver.userID['userID'] ? driver.userID['userID'] : driver.userID;
      const topic = `user-${targetID}`

      const notification = await this.notificationsService.createOne({
        createOneData: {
          title: `${currentUser.firstName} ${currentUser.lastName} chọn bạn làm tài xế`,
          data: `Bạn được chọn làm tài xế cho chuyến đi ${ vehicleBookingID}`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: targetID,
          driverID,
          type: NotificationTypes.BOOKING_SELECT_DRIVER,
        },
        credentials: user,
      })
      
      this.socketGateway.server
      .to(topic)
      .emit(
        NotificationTypes.BOOKING_SELECT_DRIVER,
        {
          data: updatedBooking,
          notification,
        }
      )      

      return updatedBooking
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('vehicleBookings/:vehicleBookingID/driver/confirm')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_DRIVER_CONFIRM]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async driverConfirm(
    @Request() { user },
    @Param() { vehicleBookingID },
    @Body() driverConfirm: DriverConfirmDto,
  ): Promise<VehicleBookings> {
    try {
      const { status } = driverConfirm

      const vehicleBooking: any = await this.vehicleBookingsService.findOne({
        query: { vehicleBookingID },
        credentials: {
          ...user,
          isAdmin: true,
        }
      })

      const targetUserID = vehicleBooking &&
            vehicleBooking.customerID &&
            vehicleBooking.customerID.userID ? vehicleBooking.customerID.userID : vehicleBooking.customerID

      if (status && status === 'denied') {
        const deniedBooking: any = await this.vehicleBookingsService.updateOne({
          updateOneData: {
            status: Status.DRIVER_DENIED,
          },
          credentials: user,
          query: { vehicleBookingID },
        })

        this.driversService.updateOne({
        query: {
          driverID: deniedBooking.driverID && deniedBooking.driverID.driverID ? deniedBooking.driverID.driverID : deniedBooking.driverID,
        },
        updateData: {
          workingStatus: 'busy',
        },
        credentials: {
          isAdmin: true,
        }
      })

        const deniedNotification = await this.notificationsService.createOne({
          createOneData: {
            title: `Chuyến đi ${vehicleBookingID}`,
            data: `Tài xế đã từ chối yêu cầu của bạn`,
            createdBy: user.userID,
            isRead: false,
            targetUserID: targetUserID,
            driverID: vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
            type: NotificationTypes.BOOKING_DRIVER_DENIED,
          },
          credentials: user,
        })


        this.socketGateway.server
        .to(`user-${targetUserID}`)
        .emit(
          NotificationTypes.BOOKING_DRIVER_DENIED,
          {
            data: deniedBooking,
            notification: deniedNotification
          }
        )
        
        return deniedBooking
      }

      const acceptedBooking: any = await this.vehicleBookingsService.updateOne({
        updateOneData: {
          status: Status.DRIVER_ACCEPTED,
        },
        credentials: user,
        query: { vehicleBookingID },
      })

      this.driversService.updateOne({
        query: {
          driverID: acceptedBooking.driverID && acceptedBooking.driverID.driverID ? acceptedBooking.driverID.driverID : acceptedBooking.driverID,
        },
        updateData: {
          workingStatus: 'busy',
        },
        credentials: {
          isAdmin: true,
        }
      })

      const notification = await this.notificationsService.createOne({
        createOneData: {
          title: `Chuyến đi ${vehicleBookingID}`,
          data: `Tài xế đã chấp nhận yêu cầu của bạn`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: targetUserID,
          driverID: vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
          type: NotificationTypes.BOOKING_DRIVER_ACCEPTED,
        },
        credentials: user,
      })

      this.socketGateway.server
      .to(`user-${targetUserID}`)
      .emit(
        NotificationTypes.BOOKING_DRIVER_ACCEPTED,
        {
          data: acceptedBooking,
          notification,
        }
      )  
      // send notification to customer

      return acceptedBooking
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('vehicleBookings/:vehicleBookingID/driverStart')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_DRIVER_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async driverStart(
    @Request() { user },
    @Param() { vehicleBookingID },
  ): Promise<boolean> {
    try {
      const vehicleBooking: any = await this.vehicleBookingsService.updateOne({
        updateOneData: { status: Status.DRIVER_IS_COMING },
        credentials: user,
        query: { vehicleBookingID },
      })

      const targetUserID = vehicleBooking &&
            vehicleBooking.customerID &&
            vehicleBooking.customerID.userID ? vehicleBooking.customerID.userID : vehicleBooking.customerID

      
      const notification = await this.notificationsService.createOne({
        createOneData: {
          title: `Chuyến đi ${vehicleBookingID}`,
          data: `Tài xế mà bạn chọn đang đến đón bạn, vui lòng đợi`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: targetUserID,
          driverID: vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
          type: NotificationTypes.DRIVER_IS_COMING,
        },
        credentials: user,
      })

      this.socketGateway.server
      .to(`user-${targetUserID}`)
      .emit(
        NotificationTypes.DRIVER_IS_COMING,
        {
          data: vehicleBooking,
          notification,
        }
      ) 

      return true
    } catch (error) {
      checkControllerErrors
    }
  }

  @Put('vehicleBookings/:vehicleBookingID/start')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_DRIVER_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  async start(
    @Request() { user },
    @Param() { vehicleBookingID },
  ): Promise<boolean> {
    try {
      const vehicleBooking: any = await this.vehicleBookingsService.updateOne({
        updateOneData: {
          status: Status.RUNNING,
          startedAt: new Date(),
        },
        credentials: user,
        query: { vehicleBookingID },
      })

      const targetUserID = vehicleBooking &&
            vehicleBooking.customerID &&
            vehicleBooking.customerID.userID ? vehicleBooking.customerID.userID : vehicleBooking.customerID

      const notification = await this.notificationsService.createOne({
        createOneData: {
          title: `Chuyến đi ${vehicleBookingID}`,
          data: `Bắt đầu hành trình`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: targetUserID,
          driverID: vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
          type: NotificationTypes.BOOKING_START,
        },
        credentials: user,
      })

      this.socketGateway.server
      .to(`user-${targetUserID}`)
      .emit(
        NotificationTypes.BOOKING_START,
        {
          data: vehicleBooking,
          notification,
        }
      ) 

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('vehicleBookings/:vehicleBookingID/finished')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_DRIVER_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  async finished(
    @Request() { user },
    @Param() { vehicleBookingID },
  ): Promise<boolean> {
    try {
      const vehicleBooking: any = await this.vehicleBookingsService.updateOne({
        updateOneData: {
          status: Status.FINISHED,
          finishedAt: new Date(),
        },
        credentials: user,
        query: { vehicleBookingID },
      })

      this.driversService.updateOne({
        query: {
          driverID: vehicleBooking.driverID && vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
        },
        updateData: {
          workingStatus: 'free',
        },
        credentials: {
          isAdmin: true,
        }
      })

      const targetUserID = vehicleBooking &&
            vehicleBooking.customerID &&
            vehicleBooking.customerID.userID ? vehicleBooking.customerID.userID : vehicleBooking.customerID

      const notification = await this.notificationsService.createOne({
        createOneData: {
          title: `Chuyến đi ${vehicleBookingID}`,
          data: `Hoàn thành hành trình`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: targetUserID,
          driverID: vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
          type: NotificationTypes.BOOKING_FINISHED,
        },
        credentials: user,
      })

      this.socketGateway.server
      .to(`user-${targetUserID}`)
      .emit(
        NotificationTypes.BOOKING_FINISHED,
        {
          data: vehicleBooking,
          notification,
        }
      ) 

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Put('vehicleBookings/:vehicleBookingID/cancel')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_UPDATE]]))
  @UseGuards(AuthGuard('jwt'))
  async cancelBooking(
    @Request() { user },
    @Param() { vehicleBookingID },
  ): Promise<boolean> {
    try {
      const [vehicleBooking, currentUser ]: any = await Promise.all([
        this.vehicleBookingsService.findOne({
          query: { vehicleBookingID, customerID: user.userID },
          credentials: user
        }),
        this.usersService.findOne({
          query: { userID: user.userID },
          credentials: user,
        })
      ])

      await this.vehicleBookingsService.updateOne({
        updateOneData: { status: Status.CANCELED },
        credentials: user,
        query: { vehicleBookingID },
      })

      const targetUserID = vehicleBooking.driverID && vehicleBooking.driverID.userID ?  vehicleBooking.driverID.userID : null

      if (!targetUserID) {
        return true
      }
      
      const notification = await this.notificationsService.createOne({
        createOneData: {
          title: `${currentUser.firstName} ${currentUser.lastName} đã hủy chuyến`,
          data: `Chuyến ${vehicleBookingID} đã bị hủy`,
          createdBy: user.userID,
          isRead: false,
          targetUserID: targetUserID,
          driverID: vehicleBooking.driverID.driverID ? vehicleBooking.driverID.driverID : vehicleBooking.driverID,
          type: NotificationTypes.BOOKING_CANCELED,
        },
        credentials: user,
      })

      this.socketGateway.server
      .to(`user-${targetUserID}`)
      .emit(
        NotificationTypes.BOOKING_CANCELED,
        {
          data: vehicleBooking,
          notification,
        }
      ) 

      return true
    } catch (error) {
      checkControllerErrors(error)
    }
  }

  @Get('vehicleBookings/:vehicleBookingID')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_READ]]))
  @UseGuards(AuthGuard('jwt'))
  async getOneBooking(
    @Request() { user },
    @Param() { vehicleBookingID }
  ): Promise<VehicleBookings> {
    try {
      const booking = await this.vehicleBookingsService.findOne({
        query: { vehicleBookingID },
        credentials: user
      })

      return booking;
    } catch (error) {
      checkControllerErrors(error)  
    }
  }

  @Get('vehicleBookings')
  @UseGuards(new Scopes([[scopes.VEHICLE_BOOKING_READ]]))
  @UseGuards(AuthGuard('jwt'))
  async getBookings(
    @Request() { user },
  ): Promise<object> {
    try {
      const userID = user.userID

      const bookings = await this.vehicleBookingsService.findMany({
        query: { customerID: userID, limit: '5', sortBy: 'createdAt', sortDirection: 'DESC' },
        credentials: user
      })

      return bookings;
    } catch (error) {
      checkControllerErrors(error)  
    }
  }
}