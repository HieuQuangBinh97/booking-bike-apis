import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigsModule } from '../../configs/configs.module'
import { DriversModule } from '../drivers/drivers.module'
import { NotificationsModule } from '../notifications/notifications.module'
import { SocketModule } from '../socket/socket.module'
import { UsersModule } from '../users/users.module'
import { VehicleBookings } from './models/vehicle_bookings.schema'
import { VehicleBookingsController } from './vehicle_bookings.controller'
import { VehicleBookingsService } from './vehicle_bookings.service'

const VehicleBookingRepository = TypeOrmModule.forFeature([VehicleBookings])

@Module({
  imports: [
    VehicleBookingRepository,
    forwardRef(() => UsersModule),
    forwardRef(() => DriversModule),
    SocketModule,
    ConfigsModule,
    NotificationsModule
  ],
  controllers: [
    VehicleBookingsController,
  ],
  exports: [
    VehicleBookingRepository,
    VehicleBookingsService,
  ],
  providers: [
    VehicleBookingsService
  ]
})

export class VehicleBookingsModule {}