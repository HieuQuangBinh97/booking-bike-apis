import { Drivers } from '../../drivers/models/drivers.schema'
import { Users } from '../../users/models/users.schema'
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne,
  PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

export interface Position {
  lat: number
  lng: number
}

export enum Status {
  PENDING = 'PENDING',
  PROCESSING = 'PROCESSING',
  RUNNING = 'RUNNING',
  DRIVER_ACCEPTED = 'DRIVER_ACCEPTED',
  FINISHED = 'FINISHED',
  CANCELED = 'CANCELED',
  DRIVER_DENIED = 'DRIVER_DENIED',
  DRIVER_IS_COMING = 'DRIVER_IS_COMING'
}


@Entity()
export class VehicleBookings {
  @PrimaryGeneratedColumn('uuid')
  vehicleBookingID: string

  @ManyToOne(() => Drivers, driver => driver.driverID, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'driverID' })
  driverID: string

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'customerID' })
  customerID: string

  @Column({ type: 'varchar', nullable: true})
  startAddress: string

  @Column({ type: 'varchar', nullable: true})
  destinationAddress: string

  @Column({ type: 'json', nullable: true})
  startingPoint: Position

  @Column({ type: 'json', nullable: true})
  destination: Position

  @Column({ type: 'float', nullable: false })
  distances: number

  @Column({ type: 'int', nullable: true, default: 0 })
  fee: number

  @Column({ type: 'int', nullable: false })
  price: number

  @Column({ type: 'datetime', nullable: true })
  startedAt: Date

  @Column({ type: 'datetime', nullable: true })
  finishedAt: Date

  @Column({ type: 'varchar', nullable: false })
  status: string

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'createdBy' })
  createdBy: string

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date
}