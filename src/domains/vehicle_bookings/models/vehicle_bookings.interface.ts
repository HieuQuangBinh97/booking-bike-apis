import { Credentials } from '../../../shared/validate_access'
import { Position, VehicleBookings } from './vehicle_bookings.schema'

interface CreateOneData {
  driverID?: string
  customerID: string
  startingPoint: Position
  destination: Position
  startAddress: string
  destinationAddress: string
  distances: number
  fee?: number
  price: number
  status: string
}

interface FindOneQuery {
  vehicleBookingID: string
  driverID?: string
  customerID?: string
}

interface UpdateOneData {
  driverID?: string
  startingPoint?: Position
  destination?: Position
  distances?: number
  fee?: number
  price?: number
  status?: string
  startedAt?: Date
  finishedAt?: Date
  updatedAt?: Date
}

interface FindManyQuery {
  limit?: string
  offset?: string
  sortBy?: string
  sortDirection?: string
  cursor?: string
  driverID?: string
  customerID?: string
  createdBy?: string
  status?: any
  vehicleBookingID?: string
}

export class CreateOneService {
  createOneData: CreateOneData
  credentials: Credentials
}

export class FindOneService {
  query: FindOneQuery
  credentials: Credentials
}

export class UpdateOneService {
  query: FindOneQuery
  updateOneData: UpdateOneData
  credentials: Credentials
}

export class FindManyService {
  query: FindManyQuery
  credentials: Credentials
}

export class DeleteOneService {
  query: FindOneQuery
  credentials: Credentials
}

export class FindManyVehicleBookings {
  totalCount: number
  list: VehicleBookings[]
  cursor: string
}