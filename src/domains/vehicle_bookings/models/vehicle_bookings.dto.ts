import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Type } from 'class-transformer'
import { IsBoolean, IsDate, IsDateString, IsEnum, IsNumber, IsOptional, IsString, Min, ValidateNested } from 'class-validator'

export class PositionDto {
  @ApiProperty()
  @IsNumber()
  readonly lat: number

  @ApiProperty()
  @IsNumber()
  readonly lng: number
}

export class BookingVehicleDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  readonly customerID?: string

  @ApiProperty({ type: PositionDto })
  @ValidateNested()
  @Type(() => PositionDto)
  readonly startingPoint: PositionDto

  @ApiProperty()
  @IsString()
  readonly startAddress: string

  @ApiProperty()
  @IsString()
  readonly destinationAddress: string

  @ApiProperty({ type: PositionDto })
  @ValidateNested()
  @Type(() => PositionDto)
  readonly destination: PositionDto

  @ApiProperty({ description: 'unit of distance is meter' })
  @IsNumber()
  @Min(100)
  readonly distances: number

  @ApiProperty()
  @IsNumber()
  @Min(0)
  readonly price: number

  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  readonly fee?: number
}

export class SelectDriverDto {
  @ApiProperty()
  @IsString()
  readonly driverID: string
}

export class DriverConfirmDto {
  @ApiProperty({ enum: ['accepted', 'denied']})
  @IsEnum(['accepted', 'denied'])
  @IsString()
  readonly status: string
}

export class UpdateBookingDto {
  @ApiPropertyOptional()
  @IsBoolean()
  @IsOptional()
  readonly isStarted?: Date

  @ApiPropertyOptional()
  @IsBoolean()
  @IsOptional()
  readonly isFinished?: Date
}