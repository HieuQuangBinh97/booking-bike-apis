import { Module } from "@nestjs/common";
import { AuthModule } from "../../middleware/auth/auth.module";
import { DriversModule } from "../drivers/drivers.module";
import { UsersModule } from "../users/users.module";
import { VehicleBookingsModule } from "../vehicle_bookings/vehicle_bookings.module";
import { AdminPageController } from "./views.controller";

@Module({
  imports: [
    VehicleBookingsModule,
    UsersModule,
    DriversModule,
    AuthModule
  ],
  controllers: [AdminPageController],
  exports: [],
  providers: []
})

export class AdminPagesModule {}