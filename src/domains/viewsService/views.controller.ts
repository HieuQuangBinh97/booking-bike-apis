import { Controller, Get, Post, Redirect, Render, Req, Res, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { Request, response, Response } from "express";
import { scopes } from "../../constants/scopes";
import { Scopes } from "../../middleware/authz/authz.service";
import { DriversService } from "../drivers/drivers.service";
import { UsersService } from "../users/users.service";
import { VehicleBookingsService } from "../vehicle_bookings/vehicle_bookings.service";
import * as bcrypt from 'bcrypt'
import { AuthService } from "../../middleware/auth/auth.service";
import { SessionStrategy } from "../../middleware/auth/strategies/session.strategy";

@Controller()
export class AdminPageController {
  constructor(
    private readonly vehicleBookingsService: VehicleBookingsService,
    private readonly usersService: UsersService,
    private readonly driversService: DriversService,
    private readonly authService: AuthService
  ) { }

  @Get('adminPage/dashboard')
  @UseGuards(new Scopes([[scopes.ADMIN_STATISTIC]]))
  @UseGuards(AuthGuard('jwt'))
  @UseGuards(new SessionStrategy())
  @Render('index.ejs')
  async getIndexPage(
    @Req() request: Request,
    @Res() response: Response,
  ) {
    try {
      const mapData = await this.vehicleBookingsService.statistic()
      const drivers = await this.driversService.findMany({
        query: {},
        credentials: { isAdmin: true }
      })

      return {
        ...mapData,
        drivers,
      }
      
    } catch (error) {
      
    }
  }

  @Get('adminPage/login')
  @Render('loginPage.ejs')
  async renderLoginPage() {
    return {
      error: null 
    }
  }

  @Post('adminPage/login')
  async handleLogin(
    @Req() request: Request,
    @Res() response: Response
  ) {
    try {
      const { phoneNumber, password } = request.body

      const existUser = await this.usersService.findOne({
        query: { phoneNumber },
        credentials: { isAdmin: true },
        getPassword: true,
      })

      if (!existUser) {
        throw {
          code: 400,
          name: 'UserNotFound'
        }
      }

      const isRightPassword = await bcrypt.compare(password, existUser.password)

      if (!isRightPassword) {
        throw {
          code: 400,
          name: 'PasswordIsInCorrect',
        }
      }

      const jwtToken = await this.authService.generateJWT(existUser)

      request.session['accessToken'] = jwtToken.accessToken

      response.redirect('/adminPage/dashboard')

      return;
    } catch (error) {
      Redirect('/adminPage/login')
    }
  }
}