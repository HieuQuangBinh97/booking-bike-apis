import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
} from '@nestjs/websockets'
import { Server, Socket } from 'socket.io'
import { verify } from 'jsonwebtoken'
import { ConfigsService } from '../../configs/configs.service'
import { InjectRepository } from '@nestjs/typeorm'
import { Users } from '../users/models/users.schema'
import { Repository, Not } from 'typeorm'
import { DriversService } from '../drivers/drivers.service'

@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    @InjectRepository(Users)
    private readonly usersRepository: Repository<Users>,
    private readonly configService: ConfigsService,
    private readonly driverService: DriversService,
  ) { }

  @WebSocketServer() server: Server

  @SubscribeMessage('HELLO')
  async handleHelloMessage(client: Socket, data) {
    try {
      console.log('hello')
      this.server.emit('HELLO', data);
    } catch (error) {
      console.log(error)
    }
  }

  @SubscribeMessage('connection')
  async handleConnection(client: Socket) {
    try {
      console.log('have new connection')
      const { headers } = client.request

      if (!headers || !headers.token) {
        return {
          status: 400,
          message: 'You must put access token into the request',
        }
      }

      const tokenData: any = verify(headers.token, this.configService.jwtSecret)

      const user = await this.usersRepository.findOne({
        where: {
          userID: tokenData.userID,
          status: Not('DELETED'),
        },
      })

      if (!user) {
        return {
          status: 400,
          message: 'Access token or Api key is invalid',
        }
      }

      try {
        await this.driverService.updateOne({
          query: { userID: user.userID },
          updateData: {
            lastLat: Number(headers.lat),
            lastLng: Number(headers.lng),
          },
          credentials: {
            isAdmin: true,
          }
        })
      } catch (error) {
        console.log(error)
      }

      client.join([
        `user-${user.userID}`,
      ])
    } catch (error) {
      console.log(error)
    }
  }

  
  @SubscribeMessage('disconnected')
  async handleDisconnect(client: Socket) {
    console.log('a client disconnected')

    if (!client.adapter.sids || !client.adapter.sids[client.id]) {
      return {
        status: 200,
        message: 'Disconnected from socket gateway',
      }
    }

    const rooms = Object.keys(client.adapter.sids[client.id])

    rooms.map(each => client.leave(each))
  }
}
