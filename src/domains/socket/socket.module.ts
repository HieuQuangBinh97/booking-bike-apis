import { forwardRef, Module } from '@nestjs/common'
import { SocketGateway } from './socket.gateway'
import { UsersModule } from '../users/users.module'
import { DriversModule } from '../drivers/drivers.module'

@Module({
  imports: [
    forwardRef(() => UsersModule),
    forwardRef(() => DriversModule),
  ],
  providers: [
    SocketGateway,
  ],
  exports: [
    SocketGateway,
  ],
})

export class SocketModule { }
