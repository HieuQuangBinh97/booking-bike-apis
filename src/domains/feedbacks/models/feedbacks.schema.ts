import { Drivers } from '../../drivers/models/drivers.schema'
import { Users } from '../../users/models/users.schema'
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne,
  PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'

@Entity()
export class Feedbacks {
  @PrimaryGeneratedColumn('uuid')
  feedbackID: string
  
  @ManyToOne(() => Drivers, driver => driver.driverID, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'driverID' })
  driverID: string

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'customerID' })
  customerID: string

  @Column({ type: 'int', default: 0 })
  numberOfRate: number

  @Column({ type: 'text', default: null})
  content: string

  @Column({ type: 'varchar', nullable: true })
  emotion: string

  @Column({ type: 'simple-array', nullable: true })
  images: string[]

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date
}