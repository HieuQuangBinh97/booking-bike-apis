import { InjectRepository } from "@nestjs/typeorm";
import { buildFindingQuery } from "../../helpers/build";
import { validateAccessToList, validateAccessToSingle } from "../../shared/validate_access";
import { EntityRepository, Repository } from "typeorm";
import { CreateOneNotificationService, FindManyNotificationsService,
  FindOneNotificationService, ResultOfFindManyNotifications } from "./models/notifications.interface";
import { Notifications, NotificationTypes } from "./models/notifications.schema";
import { getNextCursor } from "../../helpers/gets";

@EntityRepository(Notifications)
export class NotificationsService {
  constructor(
    @InjectRepository(Notifications)
    private readonly notificationsRepository: Repository<Notifications>
  ) {}

  private validateAccessToSingle = validateAccessToSingle
  private validateAccessToList = validateAccessToList

  async createOne({ createOneData, credentials }: CreateOneNotificationService): Promise<Notifications> {
    try {
      const payload = {
        ...createOneData,
        createdBy: credentials.userID
      }

      this.validateAccessToSingle({
        data: payload,
        credentials,
      })

      const newNotification = await this.notificationsRepository.save(payload)

      return newNotification
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findOne({ query, credentials }: FindOneNotificationService): Promise<Notifications> {
    try {
      const notification = await this.notificationsRepository.findOne({
        where: query,
        relations: ['driverID', 'targetUserID', 'createdBy']
      })

      if (!notification) {
        throw {
          code: 404,
          name: 'NotificationNotFound'
        }
      }

      this.validateAccessToSingle({
        data: notification,
        credentials,
      })

      return notification
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany({ query , credentials }: FindManyNotificationsService): Promise<ResultOfFindManyNotifications> {
    try {
      const { sortBy='createdAt', limit = '10', offset } = query

      const { findAllQuery, findingQuery, hasPage, sortingCondition } = buildFindingQuery({
        query: {
          ...query,
          sortBy
        }
      })

      const promises = []

      if (hasPage) {
        promises.push(
          this.notificationsRepository.find({
            where: findingQuery,
            relations: ['driverID', 'targetUserID', 'createdBy'],
            take: Number(limit),
            skip: Number(offset),
            order: sortingCondition
          }),
          this.notificationsRepository.count({
            where: findAllQuery
          })
        )
      }

      if (!hasPage) {
        promises.push(
          this.notificationsRepository.find({
            where: findAllQuery,
            relations: ['driverID', 'targetUserID', 'createdBy'],
            order: sortingCondition,
          }),
          this.notificationsRepository.count({
            where: findAllQuery
          })
        )
      }

      const [notifications, totalCount] = await Promise.all(promises)

      const { validData } = this.validateAccessToList<Notifications>({
        data: notifications,
        credentials,
      })

      const nextCursor = getNextCursor({
        data: notifications,
        sortBy,
      })

      return {
        totalCount,
        list: validData,
        cursor: nextCursor
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }
}