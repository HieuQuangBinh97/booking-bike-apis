import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { DriversModule } from "../drivers/drivers.module";
import { UsersModule } from "../users/users.module";
import { Notifications } from "./models/notifications.schema";
import { NotificationsController } from "./notifications.controller";
import { NotificationsService } from "./notifications.service";

const NotificationRepository = TypeOrmModule.forFeature([Notifications])

@Module({
  imports: [
    NotificationRepository,
    forwardRef(() => UsersModule),
    forwardRef(() => DriversModule),
  ],
  controllers: [NotificationsController],
  exports: [ NotificationRepository, NotificationsService],
  providers: [NotificationsService]
})

export class NotificationsModule {}