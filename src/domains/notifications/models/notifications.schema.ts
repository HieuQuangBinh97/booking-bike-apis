import { Feedbacks } from '../../feedbacks/models/feedbacks.schema'
import { VehicleBookings } from '../../vehicle_bookings/models/vehicle_bookings.schema'
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm'
import { Drivers } from '../../drivers/models/drivers.schema'
import { Users } from '../../users/models/users.schema'

export enum NotificationTypes {
  BOOKING_SELECT_DRIVER = 'BOOKING_SELECT_DRIVER',
  BOOKING_DRIVER_ACCEPTED = 'BOOKING_DRIVER_ACCEPTED',
  BOOKING_START = 'BOOKING_START',
  BOOKING_FINISHED = 'BOOKING_FINISHED',
  BOOKING_DRIVER_DENIED = 'BOOKING_DRIVER_DENIED',
  ADMIN_ACCEPTED_DRIVER = 'ADMIN_ACCEPTED_DRIVER',
  ADMIN_DENIED_DRIVER = 'ADMIN_DENIED_DRIVER',
  RATING_DRIVER = 'RATING_DRIVER',
  DRIVER_IS_COMING = 'DRIVER_IS_COMING',
  BOOKING_CANCELED = 'BOOKING_CANCELED'
}

@Entity()
export class Notifications {
  @PrimaryGeneratedColumn('uuid')
  notificationID: string

  @ManyToOne(() => VehicleBookings, vehicleBooking => vehicleBooking.vehicleBookingID, { nullable: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'vehicleBookingID'})
  vehicleBookingID: string

  @ManyToOne(() => Feedbacks, feedbacks => feedbacks.feedbackID, { nullable: true, onDelete: 'CASCADE'})
  @JoinColumn({ name: 'feedbackID'})
  feedbackID: string
  
  @ManyToOne(() => Drivers, driver => driver.driverID, { nullable: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'driverID' })
  driverID: string

  @ManyToOne(() => Users, user => user.userID, { nullable: true, onDelete: 'SET NULL' })
  @JoinColumn({ name: 'customerID' })
  customerID: string

  @Column({ type: 'text', nullable: true })
  data: string
  
  @Column({ type: 'varchar', nullable: false })
  title: string

  @Column({ type: 'varchar', nullable: true })
  type: string

  @Column({ type: 'simple-array', nullable: true })
  image: string[]

  @Column({ type: 'boolean', default: false })
  isRead: boolean

  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'targetUserID' })
  targetUserID: string
  
  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'createdBy' })
  createdBy: string

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date
}