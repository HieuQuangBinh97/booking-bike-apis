import { Credentials } from '../../../shared/validate_access'
import { Notifications } from './notifications.schema';

interface CreateOneData {
  vehicleBookingID?: string
  feedbackID?: string
  driverID?: string
  customerID?: string
  data: string
  title: string
  type: string
  image?: string[]
  isRead: boolean
  targetUserID: string
  createdBy: string
}

interface Query {
  notificationID: string
}

interface FindManyQuery {
  limit?: string
  offset?: string
  sortBy?: string
  sortDirection?: string
  cursor?: string
  driverID?: string
  data?: string
  title?: string
  type?: string
  isRead?: boolean
  targetUserID?: string
  createdBy?: string
}

export interface CreateOneNotificationService {
  createOneData: CreateOneData
  credentials: Credentials
}

export interface FindOneNotificationService {
  query: Query
  credentials: Credentials
}

export interface FindManyNotificationsService {
  query: FindManyQuery
  credentials: Credentials
}

export interface ResultOfFindManyNotifications {
  totalCount: number
  list: Notifications[],
  cursor: string
}