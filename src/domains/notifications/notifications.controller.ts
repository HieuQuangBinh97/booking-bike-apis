import { Controller, Get, Request, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { checkControllerErrors } from "../../helpers/check_error";
import { scopes } from "../../constants/scopes";
import { Scopes } from "../../middleware/authz/authz.service";
import { DriversService } from "../drivers/drivers.service";
import { UsersService } from "../users/users.service";
import { ResultOfFindManyNotifications } from "./models/notifications.interface";
import { NotificationsService } from "./notifications.service";

@Controller()
export class NotificationsController {
  constructor(
    private readonly notificationsService: NotificationsService,
    private readonly driversService: DriversService,
    private readonly usersService: UsersService
  ) {}

  @Get('users/me/notifications')
  @UseGuards(new Scopes([[scopes.NOTIFICATION_READ]]))
  @UseGuards(AuthGuard('jwt'))
  async getPersonalNotifications(
    @Request() { user }
  ): Promise<ResultOfFindManyNotifications> {
    try {
      let query = {
        targetUserID: user.userID,
        limit: '10',
        sortDirection: 'DESC',
      }

      const notifications = await this.notificationsService.findMany({
        query,
        credentials: user
      })

      return notifications
    } catch (error) {
      checkControllerErrors(error)
    }
  }
}