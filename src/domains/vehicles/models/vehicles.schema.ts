import { Drivers } from '../../drivers/models/drivers.schema'
import { Users } from '../../users/models/users.schema'
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne,
  PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Vehicles {
  @PrimaryGeneratedColumn('uuid')
  vehicleID: string

  @ManyToOne(() => Drivers, driver => driver.driverID, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'driverID' })
  driverID: string

  @Column({ type: 'varchar', nullable: false })
  vehicleType: string

  @Column({ type: 'varchar', nullable: false })
  region: string

  @Column({ type: 'varchar', nullable: false })
  status: string
  
  @ManyToOne(() => Users, user => user.userID, { onDelete: 'SET NULL' })
  @JoinColumn({ name: 'createdBy' })
  createdBy: string

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date

  @UpdateDateColumn({ type: 'datetime' })
  updatedAt: Date
}