import { Module } from '@nestjs/common';
import { ConfigsModule } from './configs/configs.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigsService } from './configs/configs.service';
import { UsersModule } from './domains/users/users.module';
import { AuthModule } from './middleware/auth/auth.module';
import { DriversModule } from './domains/drivers/drivers.module';
import { SocketModule } from './domains/socket/socket.module';
import { VehicleBookingsModule } from './domains/vehicle_bookings/vehicle_bookings.module';
import { AdminPagesModule } from './domains/viewsService/views.module';

const DatabaseModule = TypeOrmModule.forRootAsync({
  useFactory: async (configService: ConfigsService) => (configService.databaseConfig),
  inject: [ConfigsService],
})


@Module({
  imports: [
    SocketModule,
    ConfigsModule,
    DatabaseModule,
    UsersModule,
    AuthModule,
    DriversModule,
    VehicleBookingsModule,
    AdminPagesModule,
  ]
})
export class AppModule {}
