import * as fs from 'fs'

interface StorageFile {
  buffer: Buffer
  fileName: string,
  originalName: string
}

export const storageFile =  async ({
  buffer,
  fileName,
  originalName,
}: StorageFile): Promise<string> => {
  try {
    const ext = originalName.split('.')[1]
    const finalName = `${fileName}_${new Date().getTime()}.${ext}`

    await fs.writeFileSync(`./public/images/${finalName}`, buffer);

    return finalName
  } catch(error) {

  }
}   