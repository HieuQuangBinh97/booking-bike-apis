import { roles } from '../../constants/roles'

interface RolesAndScopes {
  roleNames: string[]
  scopeAccess: string[]
}

interface Access {
  roleName: string
  status: string
}

export const getRolesScopes = (access: Access[]): RolesAndScopes => {
  try {
    let roleNames: string[] = []
    let availableScopes: string[] = []

    if (!access || !access.length) {
      return {
        roleNames: [],
        scopeAccess: [],
      }
    }

    const activeRole = access.filter(role => role.status === 'ACTIVE')
    
    roleNames = activeRole.map(role => role.roleName)
    
    activeRole.forEach(each => {
      availableScopes.push(...roles[each.roleName])
    })
    
    const filterScopes: string[] = []
    
    availableScopes.forEach(each => {
      if (!filterScopes.includes(each)) {
        filterScopes.push(each)
      }
    })

    return {
      roleNames,
      scopeAccess: filterScopes,
    }
  } catch (error) {
    console.log(error)
  }
}

