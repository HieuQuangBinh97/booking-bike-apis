interface CheckScopes {
  scopeAccess: string[]
  requiredScopes: string[][]
}

export const checkScopes = ({ scopeAccess, requiredScopes }: CheckScopes): boolean => {

  if (!requiredScopes || !requiredScopes.length) {
    return true
  }

  const hasScopes = requiredScopes.every(requiredScope => {
    if (!requiredScope || !requiredScope.length) {
      return true
    }

    let isValid = false
    requiredScope.forEach(each => {
      if (each && scopeAccess.includes(each)) {
        isValid = true
      }
    })

    return isValid
  })

  if (!hasScopes) {
    return false
  }

  return true
}