import { CanActivate, Injectable, ExecutionContext } from '@nestjs/common'
import { checkScopes } from '../helper/check'

@Injectable()
export class Scopes implements CanActivate {
  constructor(
    private readonly requiredScopes: string[][],
  ) { }

  canActivate(context: ExecutionContext): boolean {
    const requiredScopes = this.requiredScopes

    if (!requiredScopes) {
      return true
    }

    const req = context.switchToHttp().getRequest()
    const user = req.user

    if (!user) {
      return false
    }

    if (Array.isArray(requiredScopes) && !requiredScopes.length) {
      req.user.isPublic = true

      return true
    }

    if (user.roleNames.includes('ADMIN')) {
      req.user.isAdmin = true

      return true
    }

    const hasScopes = checkScopes({
      scopeAccess: user.scopeAccess,
      requiredScopes,
    })

    if (!hasScopes) {
      return false
    }

    return true
  }

}