import { forwardRef, Module } from '@nestjs/common'
import { PassportModule } from '@nestjs/passport'
import { UsersModule } from '../../domains/users/users.module'
import { AuthService } from './auth.service'
import { JwtStrategy } from './strategies/jwt.strategy'
@Module({
  imports: [
    PassportModule,
    forwardRef(()=> UsersModule),
  ],
  providers: [
    JwtStrategy,
    AuthService,
  ],
  exports: [
    AuthService,
  ]
})
export class AuthModule { }