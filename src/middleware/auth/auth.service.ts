import { Injectable, forwardRef, Inject } from '@nestjs/common'
import { UsersService } from '../../domains/users/users.service'
import * as bcrypt from 'bcrypt'
import * as jsonwebtoken from 'jsonwebtoken'
import { ConfigsService } from '../../configs/configs.service'
import { Users } from '../../domains/users/models/users.schema'

export interface AccessToken {
  accessToken: string,
  userID: string,
}

interface TokenPayload {
  userID: string
  changePasswordAt: Date
  status: string
}

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly configsService: ConfigsService,
  ) { }

  // async validateUser(email: string, password: string): Promise<Users | null> {
  //   const user = await this.usersService.findOne({ query: { email } })
  //   const isValid = user && user.password && await bcrypt.compare(password, user.password)

  //   if (isValid) {
  //     delete user.password

  //     return user
  //   }

  //   return null
  // }

  async generateInactivateJWT(user: Users): Promise<AccessToken> {
    const payload = {
      userID: user.userID,
      changePasswordAt: user.changePasswordAt,
      status: 'INACTIVE',
    }

    const accessToken = jsonwebtoken.sign(
      payload,
      this.configsService.jwtConfirmSecret,
      { expiresIn: '7d' },
    )

    return {
      accessToken,
      userID: user.userID
    }
  }

  async generateJWT(user: Users): Promise<AccessToken> {
    const payload: TokenPayload = {
      userID: user.userID,
      changePasswordAt: user.changePasswordAt,
      status: user.status,
    }

    const accessToken = jsonwebtoken.sign(
      payload,
      this.configsService.jwtSecret,
      { expiresIn: '180d' },
    )

    return {
      accessToken,
      userID: user.userID
    }
  }

  // async validateGoogleUser(query): Promise<Users | null> {
  //   try {
  //     const user = await this.usersService.getCurrentUserCredentials(query)

  //     if (user) {
  //       delete user.password

  //       return user
  //     }

  //     return null
  //   } catch (error) {
  //     return Promise.reject(error)
  //   }
  // }
}
