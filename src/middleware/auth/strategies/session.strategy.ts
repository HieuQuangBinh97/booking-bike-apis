import { ExtractJwt, Strategy } from 'passport-jwt'
import { AuthGuard, PassportStrategy } from '@nestjs/passport'
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigsService } from '../../../configs/configs.service'
import { getRolesScopes } from '../../helper/gets'
import { UsersService } from '../../../domains/users/users.service'
import { UserCombinedService } from '../../../domains/users/combined.service'
import { Observable } from 'rxjs'
import { request } from 'express'
import { access } from 'fs'

interface Payload {
  userID: string
  changePasswordAt: number
  status?: string
}

@Injectable()
export class SessionStrategy implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest()
    const res = context.switchToHttp().getResponse()

    const { session } = req

    if (!session) {
      return await res.redirect('/adminPage/login')
    }

    const { accessToken } = session

    if (!accessToken) {
      return await res.redirect('/adminPage/login')
    }

    req.headers.authorization = `Bearer ${accessToken}`

    return true
  }

}
