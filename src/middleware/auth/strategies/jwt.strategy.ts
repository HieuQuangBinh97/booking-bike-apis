import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigsService } from '../../../configs/configs.service'
import { getRolesScopes } from '../../helper/gets'
import { UsersService } from '../../../domains/users/users.service'
import { UserCombinedService } from '../../../domains/users/combined.service'

interface Payload {
  userID: string
  changePasswordAt: number
  status?: string
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configsService: ConfigsService,
    private readonly userCombinedService: UserCombinedService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configsService.jwtSecret,
    })
  }

  // returned object is bound to req.user
  async validate(payload: Payload): Promise<object> {
    try {
      const userData = await this.userCombinedService.getUserCredentials({
        userID: payload.userID,
        credentials: { isAdmin: true }
      })

      if (!userData) {
        throw new UnauthorizedException()
      }

      if (userData.status === 'INACTIVE') {
        throw new UnauthorizedException('You need to activate your account by confirming your email')
      }

      if (userData.changePasswordAt && userData.changePasswordAt.toISOString() !== payload.changePasswordAt) {
        throw new UnauthorizedException('Your token is expired')
      }

      const { roleNames, scopeAccess } = getRolesScopes(userData.access)

      return {
        userID: userData.userID,
        email: userData.email,
        scopeAccess,
        roleNames,
        changePasswordAt: userData.changePasswordAt,
        status: userData.status,
        driverIDs: userData.driverIDs,
      }
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
