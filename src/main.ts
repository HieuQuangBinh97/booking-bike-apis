import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ConfigsService } from './configs/configs.service';
import { join } from 'path'
import * as session from 'express-session';

const configsService = new ConfigsService()

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(
    session({
      secret: 'my-secret',
      resave: false,
      saveUninitialized: false,
    }),
  );

  app.useStaticAssets(join(__dirname, '../../', 'public'), {
    prefix: '/public/',
  });
  app.setBaseViewsDir(join(__dirname, '../../', 'views'));
  app.setViewEngine('ejs');

  const options = new DocumentBuilder()
      .setTitle('Booking bike')
      .setDescription(`Apis`)
      .setVersion('1.0')
      .build()
    const document = SwaggerModule.createDocument(app, options)
    SwaggerModule.setup('api', app, document)


  await app.listen(configsService.port);

  console.log(`our app is started on ${configsService.host}:${configsService.port}`)
}
bootstrap();
